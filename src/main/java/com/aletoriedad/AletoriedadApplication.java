package com.aletoriedad;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * = AletoriedadApplication
 *
 * TODO Auto-generated class documentation
 *
 */
@SpringBootApplication
public class AletoriedadApplication {

    /**
     * TODO Auto-generated method documentation
     *
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(AletoriedadApplication.class, args);
    }
}