package com.aletoriedad.config.jackson;
import org.springframework.roo.addon.web.mvc.controller.annotations.config.RooDomainModelModule;
import com.aletoriedad.model.CuponCompra;
import com.aletoriedad.model.Ingrediente;
import com.aletoriedad.model.IngredientePlato;
import com.aletoriedad.model.Persona;
import com.aletoriedad.model.Plato;
import com.aletoriedad.model.Restaurante;
import com.aletoriedad.model.Usuario;
import com.aletoriedad.web.CuponCompraJsonMixin;
import com.aletoriedad.web.IngredienteJsonMixin;
import com.aletoriedad.web.IngredientePlatoJsonMixin;
import com.aletoriedad.web.PersonaJsonMixin;
import com.aletoriedad.web.PlatoJsonMixin;
import com.aletoriedad.web.RestauranteJsonMixin;
import com.aletoriedad.web.UsuarioJsonMixin;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.boot.jackson.JsonComponent;

/**
 * = DomainModelModule
 *
 * TODO Auto-generated class documentation
 *
 */
@RooDomainModelModule
@JsonComponent
public class DomainModelModule extends SimpleModule {

    /**
     * TODO Auto-generated constructor documentation
     *
     */
    public DomainModelModule() {
        // Mixin registration
        setMixInAnnotation(CuponCompra.class, CuponCompraJsonMixin.class);
        setMixInAnnotation(Ingrediente.class, IngredienteJsonMixin.class);
        setMixInAnnotation(IngredientePlato.class, IngredientePlatoJsonMixin.class);
        setMixInAnnotation(Persona.class, PersonaJsonMixin.class);
        setMixInAnnotation(Plato.class, PlatoJsonMixin.class);
        setMixInAnnotation(Restaurante.class, RestauranteJsonMixin.class);
        setMixInAnnotation(Usuario.class, UsuarioJsonMixin.class);
    }
}
