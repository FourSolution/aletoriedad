package com.aletoriedad.model;
import org.springframework.roo.addon.javabean.annotations.RooEquals;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.jpa.annotations.entity.RooJpaEntity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import org.springframework.roo.addon.jpa.annotations.entity.JpaRelationType;
import org.springframework.roo.addon.jpa.annotations.entity.RooJpaRelation;
import io.springlets.format.EntityFormat;
import java.util.Objects;
import javax.persistence.Entity;
import org.springframework.util.Assert;

/**
 * = Ingrediente
 *
 * TODO Auto-generated class documentation
 *
 */
@RooJavaBean
@RooToString
@RooJpaEntity(entityFormatExpression = "#{nombre}")
@RooEquals(isJpaEntity = true)
@Entity
@EntityFormat("#{nombre}")
public class Ingrediente {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    @Version
    private Integer version;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    @NotNull
    @Size(max = 20)
    private String nombre;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    @NotNull
    @Size(max = 200)
    private String descripcion;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private Boolean excluible;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    @OneToMany(cascade = { javax.persistence.CascadeType.MERGE, javax.persistence.CascadeType.PERSIST }, fetch = FetchType.LAZY, mappedBy = "ingrediente")
    @RooJpaRelation(type = JpaRelationType.AGGREGATION)
    private Set<IngredientePlato> platosingredientes = new HashSet<IngredientePlato>();

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    public static final String ITERABLE_TO_ADD_CANT_BE_NULL_MESSAGE = "The given Iterable of items to add can't be null!";

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    public static final String ITERABLE_TO_REMOVE_CANT_BE_NULL_MESSAGE = "The given Iterable of items to add can't be null!";

    /**
     * This `equals` implementation is specific for JPA entities and uses
     * the entity identifier for it, following the article in
     * https://vladmihalcea.com/2016/06/06/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
     *
     * @param obj
     * @return Boolean
     */
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        // instanceof is false if the instance is null
        if (!(obj instanceof Ingrediente)) {
            return false;
        }
        return getId() != null && Objects.equals(getId(), ((Ingrediente) obj).getId());
    }

    /**
     * This `hashCode` implementation is specific for JPA entities and uses a fixed `int` value to be able
     * to identify the entity in collections after a new id is assigned to the entity, following the article in
     * https://vladmihalcea.com/2016/06/06/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
     *
     * @return Integer
     */
    public int hashCode() {
        return 31;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Long
     */
    public Long getId() {
        return this.id;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Integer
     */
    public Integer getVersion() {
        return this.version;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param version
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return String
     */
    public String getNombre() {
        return this.nombre;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return String
     */
    public String getDescripcion() {
        return this.descripcion;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param descripcion
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Boolean
     */
    public Boolean getExcluible() {
        return this.excluible;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param excluible
     */
    public void setExcluible(Boolean excluible) {
        this.excluible = excluible;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Set
     */
    public Set<IngredientePlato> getPlatosingredientes() {
        return this.platosingredientes;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param platosingredientes
     */
    public void setPlatosingredientes(Set<IngredientePlato> platosingredientes) {
        this.platosingredientes = platosingredientes;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return String
     */
    public String toString() {
        return "Ingrediente {" + "id='" + id + '\'' + ", version='" + version + '\'' + ", nombre='" + nombre + '\'' + ", descripcion='" + descripcion + '\'' + ", excluible='" + excluible + '\'' + ", ITERABLE_TO_ADD_CANT_BE_NULL_MESSAGE='" + ITERABLE_TO_ADD_CANT_BE_NULL_MESSAGE + '\'' + ", ITERABLE_TO_REMOVE_CANT_BE_NULL_MESSAGE='" + ITERABLE_TO_REMOVE_CANT_BE_NULL_MESSAGE + '\'' + "}" + super.toString();
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param platosingredientesToAdd
     */
    public void addToPlatosingredientes(Iterable<IngredientePlato> platosingredientesToAdd) {
        Assert.notNull(platosingredientesToAdd, ITERABLE_TO_ADD_CANT_BE_NULL_MESSAGE);
        for (IngredientePlato item : platosingredientesToAdd) {
            this.platosingredientes.add(item);
            item.setIngrediente(this);
        }
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param platosingredientesToRemove
     */
    public void removeFromPlatosingredientes(Iterable<IngredientePlato> platosingredientesToRemove) {
        Assert.notNull(platosingredientesToRemove, ITERABLE_TO_REMOVE_CANT_BE_NULL_MESSAGE);
        for (IngredientePlato item : platosingredientesToRemove) {
            this.platosingredientes.remove(item);
            item.setIngrediente(null);
        }
    }
}
