package com.aletoriedad.model;
import org.springframework.roo.addon.javabean.annotations.RooEquals;
import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.jpa.annotations.entity.RooJpaEntity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;
import io.springlets.format.EntityFormat;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.OneToMany;
import org.springframework.roo.addon.jpa.annotations.entity.JpaRelationType;
import org.springframework.roo.addon.jpa.annotations.entity.RooJpaRelation;
import java.util.Objects;
import javax.persistence.Entity;
import org.springframework.util.Assert;

/**
 * = Plato
 *
 * TODO Auto-generated class documentation
 *
 */
@RooJavaBean
@RooToString
@RooJpaEntity(entityFormatExpression = "#{nombre}")
@RooEquals(isJpaEntity = true)
@Entity
@EntityFormat("#{nombre}")
public class Plato {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    @Version
    private Integer version;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    @ManyToOne(fetch = FetchType.LAZY)
    @EntityFormat
    private Restaurante restaurante;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    @NotNull
    @Size(max = 20)
    private String nombre;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    @NotNull
    @Size(max = 200)
    private String descripcion;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    @OneToMany(cascade = { javax.persistence.CascadeType.MERGE, javax.persistence.CascadeType.PERSIST }, fetch = FetchType.LAZY, mappedBy = "plato")
    @RooJpaRelation(type = JpaRelationType.AGGREGATION)
    private Set<CuponCompra> platocupon = new HashSet<CuponCompra>();

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    @OneToMany(cascade = { javax.persistence.CascadeType.MERGE, javax.persistence.CascadeType.PERSIST }, fetch = FetchType.LAZY, mappedBy = "plato")
    @RooJpaRelation(type = JpaRelationType.AGGREGATION)
    private Set<IngredientePlato> ingredientesplatos = new HashSet<IngredientePlato>();

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    public static final String ITERABLE_TO_ADD_CANT_BE_NULL_MESSAGE = "The given Iterable of items to add can't be null!";

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    public static final String ITERABLE_TO_REMOVE_CANT_BE_NULL_MESSAGE = "The given Iterable of items to add can't be null!";

    /**
     * This `equals` implementation is specific for JPA entities and uses
     * the entity identifier for it, following the article in
     * https://vladmihalcea.com/2016/06/06/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
     *
     * @param obj
     * @return Boolean
     */
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        // instanceof is false if the instance is null
        if (!(obj instanceof Plato)) {
            return false;
        }
        return getId() != null && Objects.equals(getId(), ((Plato) obj).getId());
    }

    /**
     * This `hashCode` implementation is specific for JPA entities and uses a fixed `int` value to be able
     * to identify the entity in collections after a new id is assigned to the entity, following the article in
     * https://vladmihalcea.com/2016/06/06/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
     *
     * @return Integer
     */
    public int hashCode() {
        return 31;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Long
     */
    public Long getId() {
        return this.id;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Integer
     */
    public Integer getVersion() {
        return this.version;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param version
     */
    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Restaurante
     */
    public Restaurante getRestaurante() {
        return this.restaurante;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param restaurante
     */
    public void setRestaurante(Restaurante restaurante) {
        this.restaurante = restaurante;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return String
     */
    public String getNombre() {
        return this.nombre;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return String
     */
    public String getDescripcion() {
        return this.descripcion;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param descripcion
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Set
     */
    public Set<CuponCompra> getPlatocupon() {
        return this.platocupon;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param platocupon
     */
    public void setPlatocupon(Set<CuponCompra> platocupon) {
        this.platocupon = platocupon;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Set
     */
    public Set<IngredientePlato> getIngredientesplatos() {
        return this.ingredientesplatos;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingredientesplatos
     */
    public void setIngredientesplatos(Set<IngredientePlato> ingredientesplatos) {
        this.ingredientesplatos = ingredientesplatos;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return String
     */
    public String toString() {
        return "Plato {" + "id='" + id + '\'' + ", version='" + version + '\'' + ", nombre='" + nombre + '\'' + ", descripcion='" + descripcion + '\'' + ", ITERABLE_TO_ADD_CANT_BE_NULL_MESSAGE='" + ITERABLE_TO_ADD_CANT_BE_NULL_MESSAGE + '\'' + ", ITERABLE_TO_REMOVE_CANT_BE_NULL_MESSAGE='" + ITERABLE_TO_REMOVE_CANT_BE_NULL_MESSAGE + '\'' + "}" + super.toString();
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param platocuponToAdd
     */
    public void addToPlatocupon(Iterable<CuponCompra> platocuponToAdd) {
        Assert.notNull(platocuponToAdd, ITERABLE_TO_ADD_CANT_BE_NULL_MESSAGE);
        for (CuponCompra item : platocuponToAdd) {
            this.platocupon.add(item);
            item.setPlato(this);
        }
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param platocuponToRemove
     */
    public void removeFromPlatocupon(Iterable<CuponCompra> platocuponToRemove) {
        Assert.notNull(platocuponToRemove, ITERABLE_TO_REMOVE_CANT_BE_NULL_MESSAGE);
        for (CuponCompra item : platocuponToRemove) {
            this.platocupon.remove(item);
            item.setPlato(null);
        }
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingredientesplatosToAdd
     */
    public void addToIngredientesplatos(Iterable<IngredientePlato> ingredientesplatosToAdd) {
        Assert.notNull(ingredientesplatosToAdd, ITERABLE_TO_ADD_CANT_BE_NULL_MESSAGE);
        for (IngredientePlato item : ingredientesplatosToAdd) {
            this.ingredientesplatos.add(item);
            item.setPlato(this);
        }
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingredientesplatosToRemove
     */
    public void removeFromIngredientesplatos(Iterable<IngredientePlato> ingredientesplatosToRemove) {
        Assert.notNull(ingredientesplatosToRemove, ITERABLE_TO_REMOVE_CANT_BE_NULL_MESSAGE);
        for (IngredientePlato item : ingredientesplatosToRemove) {
            this.ingredientesplatos.remove(item);
            item.setPlato(null);
        }
    }
}
