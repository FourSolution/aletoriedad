package com.aletoriedad.repository;
import com.aletoriedad.model.CuponCompra;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepository;
import com.aletoriedad.model.Plato;
import com.aletoriedad.model.Usuario;
import io.springlets.data.jpa.repository.DetachableJpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * = CuponCompraRepository
 *
 * TODO Auto-generated class documentation
 *
 */
@RooJpaRepository(entity = CuponCompra.class)
@Transactional(readOnly = true)
public interface CuponCompraRepository extends DetachableJpaRepository<CuponCompra, Long>, CuponCompraRepositoryCustom {

    /**
     * TODO Auto-generated method documentation
     *
     * @param usuario
     * @return Long
     */
    public abstract long countByUsuario(Usuario usuario);

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @return Long
     */
    public abstract long countByPlato(Plato plato);
}
