package com.aletoriedad.repository;
import com.aletoriedad.model.CuponCompra;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepositoryCustom;
import com.aletoriedad.model.Plato;
import com.aletoriedad.model.Usuario;
import io.springlets.data.domain.GlobalSearch;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * = CuponCompraRepositoryCustom
 *
 * TODO Auto-generated class documentation
 *
 */
@RooJpaRepositoryCustom(entity = CuponCompra.class)
public interface CuponCompraRepositoryCustom {

    /**
     * TODO Auto-generated method documentation
     *
     * @param usuario
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public abstract Page<CuponCompra> findByUsuario(Usuario usuario, GlobalSearch globalSearch, Pageable pageable);

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public abstract Page<CuponCompra> findByPlato(Plato plato, GlobalSearch globalSearch, Pageable pageable);

    /**
     * TODO Auto-generated method documentation
     *
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public abstract Page<CuponCompra> findAll(GlobalSearch globalSearch, Pageable pageable);
}
