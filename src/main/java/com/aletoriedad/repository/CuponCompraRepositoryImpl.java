package com.aletoriedad.repository;
import io.springlets.data.jpa.repository.support.QueryDslRepositorySupportExt;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepositoryCustomImpl;
import com.aletoriedad.model.CuponCompra;
import com.aletoriedad.model.Plato;
import com.aletoriedad.model.QCuponCompra;
import com.aletoriedad.model.Usuario;
import com.querydsl.core.types.Path;
import com.querydsl.jpa.JPQLQuery;
import io.springlets.data.domain.GlobalSearch;
import io.springlets.data.jpa.repository.support.QueryDslRepositorySupportExt.AttributeMappingBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 * = CuponCompraRepositoryImpl
 *
 * TODO Auto-generated class documentation
 *
 */
@RooJpaRepositoryCustomImpl(repository = CuponCompraRepositoryCustom.class)
@Transactional(readOnly = true)
public class CuponCompraRepositoryImpl extends QueryDslRepositorySupportExt<CuponCompra> implements CuponCompraRepositoryCustom {

    /**
     * TODO Auto-generated constructor documentation
     */
    CuponCompraRepositoryImpl() {
        super(CuponCompra.class);
    }

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    public static final String PLATO = "plato";

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    public static final String USUARIO = "usuario";

    /**
     * TODO Auto-generated method documentation
     *
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<CuponCompra> findAll(GlobalSearch globalSearch, Pageable pageable) {
        QCuponCompra cuponCompra = QCuponCompra.cuponCompra;
        JPQLQuery<CuponCompra> query = from(cuponCompra);
        Path<?>[] paths = new Path<?>[] { cuponCompra.usuario, cuponCompra.plato };
        applyGlobalSearch(globalSearch, query, paths);
        AttributeMappingBuilder mapping = buildMapper().map(USUARIO, cuponCompra.usuario).map(PLATO, cuponCompra.plato);
        applyPagination(pageable, query, mapping);
        applyOrderById(query);
        return loadPage(query, pageable, cuponCompra);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<CuponCompra> findByPlato(Plato plato, GlobalSearch globalSearch, Pageable pageable) {
        QCuponCompra cuponCompra = QCuponCompra.cuponCompra;
        JPQLQuery<CuponCompra> query = from(cuponCompra);
        Assert.notNull(plato, "plato is required");
        query.where(cuponCompra.plato.eq(plato));
        Path<?>[] paths = new Path<?>[] { cuponCompra.usuario, cuponCompra.plato };
        applyGlobalSearch(globalSearch, query, paths);
        AttributeMappingBuilder mapping = buildMapper().map(USUARIO, cuponCompra.usuario).map(PLATO, cuponCompra.plato);
        applyPagination(pageable, query, mapping);
        applyOrderById(query);
        return loadPage(query, pageable, cuponCompra);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param usuario
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<CuponCompra> findByUsuario(Usuario usuario, GlobalSearch globalSearch, Pageable pageable) {
        QCuponCompra cuponCompra = QCuponCompra.cuponCompra;
        JPQLQuery<CuponCompra> query = from(cuponCompra);
        Assert.notNull(usuario, "usuario is required");
        query.where(cuponCompra.usuario.eq(usuario));
        Path<?>[] paths = new Path<?>[] { cuponCompra.usuario, cuponCompra.plato };
        applyGlobalSearch(globalSearch, query, paths);
        AttributeMappingBuilder mapping = buildMapper().map(USUARIO, cuponCompra.usuario).map(PLATO, cuponCompra.plato);
        applyPagination(pageable, query, mapping);
        applyOrderById(query);
        return loadPage(query, pageable, cuponCompra);
    }
}
