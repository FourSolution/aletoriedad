package com.aletoriedad.repository;
import com.aletoriedad.model.IngredientePlato;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepository;
import com.aletoriedad.model.Ingrediente;
import com.aletoriedad.model.Plato;
import io.springlets.data.jpa.repository.DetachableJpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * = IngredientePlatoRepository
 *
 * TODO Auto-generated class documentation
 *
 */
@RooJpaRepository(entity = IngredientePlato.class)
@Transactional(readOnly = true)
public interface IngredientePlatoRepository extends DetachableJpaRepository<IngredientePlato, Long>, IngredientePlatoRepositoryCustom {

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @return Long
     */
    public abstract long countByPlato(Plato plato);

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingrediente
     * @return Long
     */
    public abstract long countByIngrediente(Ingrediente ingrediente);
}
