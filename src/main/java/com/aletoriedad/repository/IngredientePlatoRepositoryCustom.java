package com.aletoriedad.repository;
import com.aletoriedad.model.IngredientePlato;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepositoryCustom;
import com.aletoriedad.model.Ingrediente;
import com.aletoriedad.model.Plato;
import io.springlets.data.domain.GlobalSearch;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * = IngredientePlatoRepositoryCustom
 *
 * TODO Auto-generated class documentation
 *
 */
@RooJpaRepositoryCustom(entity = IngredientePlato.class)
public interface IngredientePlatoRepositoryCustom {

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public abstract Page<IngredientePlato> findByPlato(Plato plato, GlobalSearch globalSearch, Pageable pageable);

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingrediente
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public abstract Page<IngredientePlato> findByIngrediente(Ingrediente ingrediente, GlobalSearch globalSearch, Pageable pageable);

    /**
     * TODO Auto-generated method documentation
     *
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public abstract Page<IngredientePlato> findAll(GlobalSearch globalSearch, Pageable pageable);
}
