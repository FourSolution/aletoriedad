package com.aletoriedad.repository;
import io.springlets.data.jpa.repository.support.QueryDslRepositorySupportExt;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepositoryCustomImpl;
import com.aletoriedad.model.IngredientePlato;
import com.aletoriedad.model.Ingrediente;
import com.aletoriedad.model.Plato;
import com.aletoriedad.model.QIngredientePlato;
import com.querydsl.core.types.Path;
import com.querydsl.jpa.JPQLQuery;
import io.springlets.data.domain.GlobalSearch;
import io.springlets.data.jpa.repository.support.QueryDslRepositorySupportExt.AttributeMappingBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 * = IngredientePlatoRepositoryImpl
 *
 * TODO Auto-generated class documentation
 *
 */
@RooJpaRepositoryCustomImpl(repository = IngredientePlatoRepositoryCustom.class)
@Transactional(readOnly = true)
public class IngredientePlatoRepositoryImpl extends QueryDslRepositorySupportExt<IngredientePlato> implements IngredientePlatoRepositoryCustom {

    /**
     * TODO Auto-generated constructor documentation
     */
    IngredientePlatoRepositoryImpl() {
        super(IngredientePlato.class);
    }

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    public static final String PLATO = "plato";

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    public static final String INGREDIENTE = "ingrediente";

    /**
     * TODO Auto-generated method documentation
     *
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<IngredientePlato> findAll(GlobalSearch globalSearch, Pageable pageable) {
        QIngredientePlato ingredientePlato = QIngredientePlato.ingredientePlato;
        JPQLQuery<IngredientePlato> query = from(ingredientePlato);
        Path<?>[] paths = new Path<?>[] { ingredientePlato.plato, ingredientePlato.ingrediente };
        applyGlobalSearch(globalSearch, query, paths);
        AttributeMappingBuilder mapping = buildMapper().map(PLATO, ingredientePlato.plato).map(INGREDIENTE, ingredientePlato.ingrediente);
        applyPagination(pageable, query, mapping);
        applyOrderById(query);
        return loadPage(query, pageable, ingredientePlato);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingrediente
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<IngredientePlato> findByIngrediente(Ingrediente ingrediente, GlobalSearch globalSearch, Pageable pageable) {
        QIngredientePlato ingredientePlato = QIngredientePlato.ingredientePlato;
        JPQLQuery<IngredientePlato> query = from(ingredientePlato);
        Assert.notNull(ingrediente, "ingrediente is required");
        query.where(ingredientePlato.ingrediente.eq(ingrediente));
        Path<?>[] paths = new Path<?>[] { ingredientePlato.plato, ingredientePlato.ingrediente };
        applyGlobalSearch(globalSearch, query, paths);
        AttributeMappingBuilder mapping = buildMapper().map(PLATO, ingredientePlato.plato).map(INGREDIENTE, ingredientePlato.ingrediente);
        applyPagination(pageable, query, mapping);
        applyOrderById(query);
        return loadPage(query, pageable, ingredientePlato);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<IngredientePlato> findByPlato(Plato plato, GlobalSearch globalSearch, Pageable pageable) {
        QIngredientePlato ingredientePlato = QIngredientePlato.ingredientePlato;
        JPQLQuery<IngredientePlato> query = from(ingredientePlato);
        Assert.notNull(plato, "plato is required");
        query.where(ingredientePlato.plato.eq(plato));
        Path<?>[] paths = new Path<?>[] { ingredientePlato.plato, ingredientePlato.ingrediente };
        applyGlobalSearch(globalSearch, query, paths);
        AttributeMappingBuilder mapping = buildMapper().map(PLATO, ingredientePlato.plato).map(INGREDIENTE, ingredientePlato.ingrediente);
        applyPagination(pageable, query, mapping);
        applyOrderById(query);
        return loadPage(query, pageable, ingredientePlato);
    }
}
