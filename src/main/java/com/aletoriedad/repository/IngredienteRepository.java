package com.aletoriedad.repository;
import com.aletoriedad.model.Ingrediente;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepository;
import io.springlets.data.jpa.repository.DetachableJpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * = IngredienteRepository
 *
 * TODO Auto-generated class documentation
 *
 */
@RooJpaRepository(entity = Ingrediente.class)
@Transactional(readOnly = true)
public interface IngredienteRepository extends DetachableJpaRepository<Ingrediente, Long>, IngredienteRepositoryCustom {
}
