package com.aletoriedad.repository;
import io.springlets.data.jpa.repository.support.QueryDslRepositorySupportExt;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepositoryCustomImpl;
import com.aletoriedad.model.Ingrediente;
import com.aletoriedad.model.QIngrediente;
import com.querydsl.core.types.Path;
import com.querydsl.jpa.JPQLQuery;
import io.springlets.data.domain.GlobalSearch;
import io.springlets.data.jpa.repository.support.QueryDslRepositorySupportExt.AttributeMappingBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

/**
 * = IngredienteRepositoryImpl
 *
 * TODO Auto-generated class documentation
 *
 */
@RooJpaRepositoryCustomImpl(repository = IngredienteRepositoryCustom.class)
@Transactional(readOnly = true)
public class IngredienteRepositoryImpl extends QueryDslRepositorySupportExt<Ingrediente> implements IngredienteRepositoryCustom {

    /**
     * TODO Auto-generated constructor documentation
     */
    IngredienteRepositoryImpl() {
        super(Ingrediente.class);
    }

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    public static final String EXCLUIBLE = "excluible";

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    public static final String DESCRIPCION = "descripcion";

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    public static final String NOMBRE = "nombre";

    /**
     * TODO Auto-generated method documentation
     *
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<Ingrediente> findAll(GlobalSearch globalSearch, Pageable pageable) {
        QIngrediente ingrediente = QIngrediente.ingrediente;
        JPQLQuery<Ingrediente> query = from(ingrediente);
        Path<?>[] paths = new Path<?>[] { ingrediente.nombre, ingrediente.descripcion, ingrediente.excluible };
        applyGlobalSearch(globalSearch, query, paths);
        AttributeMappingBuilder mapping = buildMapper().map(NOMBRE, ingrediente.nombre).map(DESCRIPCION, ingrediente.descripcion).map(EXCLUIBLE, ingrediente.excluible);
        applyPagination(pageable, query, mapping);
        applyOrderById(query);
        return loadPage(query, pageable, ingrediente);
    }
}
