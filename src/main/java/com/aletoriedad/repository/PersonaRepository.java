package com.aletoriedad.repository;
import com.aletoriedad.model.Persona;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepository;
import io.springlets.data.jpa.repository.DetachableJpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * = PersonaRepository
 *
 * TODO Auto-generated class documentation
 *
 */
@RooJpaRepository(entity = Persona.class)
@Transactional(readOnly = true)
public interface PersonaRepository extends DetachableJpaRepository<Persona, Long>, PersonaRepositoryCustom {
}
