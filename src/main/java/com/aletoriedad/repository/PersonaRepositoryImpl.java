package com.aletoriedad.repository;
import io.springlets.data.jpa.repository.support.QueryDslRepositorySupportExt;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepositoryCustomImpl;
import com.aletoriedad.model.Persona;
import com.aletoriedad.model.QPersona;
import com.querydsl.core.types.Path;
import com.querydsl.jpa.JPQLQuery;
import io.springlets.data.domain.GlobalSearch;
import io.springlets.data.jpa.repository.support.QueryDslRepositorySupportExt.AttributeMappingBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

/**
 * = PersonaRepositoryImpl
 *
 * TODO Auto-generated class documentation
 *
 */
@RooJpaRepositoryCustomImpl(repository = PersonaRepositoryCustom.class)
@Transactional(readOnly = true)
public class PersonaRepositoryImpl extends QueryDslRepositorySupportExt<Persona> implements PersonaRepositoryCustom {

    /**
     * TODO Auto-generated constructor documentation
     */
    PersonaRepositoryImpl() {
        super(Persona.class);
    }

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    public static final String IDENTIFICACION = "identificacion";

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    public static final String NOMBRE = "nombre";

    /**
     * TODO Auto-generated method documentation
     *
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<Persona> findAll(GlobalSearch globalSearch, Pageable pageable) {
        QPersona persona = QPersona.persona;
        JPQLQuery<Persona> query = from(persona);
        Path<?>[] paths = new Path<?>[] { persona.nombre, persona.identificacion };
        applyGlobalSearch(globalSearch, query, paths);
        AttributeMappingBuilder mapping = buildMapper().map(NOMBRE, persona.nombre).map(IDENTIFICACION, persona.identificacion);
        applyPagination(pageable, query, mapping);
        applyOrderById(query);
        return loadPage(query, pageable, persona);
    }
}
