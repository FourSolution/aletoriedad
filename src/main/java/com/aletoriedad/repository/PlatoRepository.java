package com.aletoriedad.repository;
import com.aletoriedad.model.Plato;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepository;
import com.aletoriedad.model.Restaurante;
import io.springlets.data.jpa.repository.DetachableJpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * = PlatoRepository
 *
 * TODO Auto-generated class documentation
 *
 */
@RooJpaRepository(entity = Plato.class)
@Transactional(readOnly = true)
public interface PlatoRepository extends DetachableJpaRepository<Plato, Long>, PlatoRepositoryCustom {

    /**
     * TODO Auto-generated method documentation
     *
     * @param restaurante
     * @return Long
     */
    public abstract long countByRestaurante(Restaurante restaurante);
}
