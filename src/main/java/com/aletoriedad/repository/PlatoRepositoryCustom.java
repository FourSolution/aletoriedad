package com.aletoriedad.repository;
import com.aletoriedad.model.Plato;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepositoryCustom;
import com.aletoriedad.model.Restaurante;
import io.springlets.data.domain.GlobalSearch;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * = PlatoRepositoryCustom
 *
 * TODO Auto-generated class documentation
 *
 */
@RooJpaRepositoryCustom(entity = Plato.class)
public interface PlatoRepositoryCustom {

    /**
     * TODO Auto-generated method documentation
     *
     * @param restaurante
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public abstract Page<Plato> findByRestaurante(Restaurante restaurante, GlobalSearch globalSearch, Pageable pageable);

    /**
     * TODO Auto-generated method documentation
     *
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public abstract Page<Plato> findAll(GlobalSearch globalSearch, Pageable pageable);
}
