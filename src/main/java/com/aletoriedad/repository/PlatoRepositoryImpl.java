package com.aletoriedad.repository;
import io.springlets.data.jpa.repository.support.QueryDslRepositorySupportExt;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepositoryCustomImpl;
import com.aletoriedad.model.Plato;
import com.aletoriedad.model.QPlato;
import com.aletoriedad.model.Restaurante;
import com.querydsl.core.types.Path;
import com.querydsl.jpa.JPQLQuery;
import io.springlets.data.domain.GlobalSearch;
import io.springlets.data.jpa.repository.support.QueryDslRepositorySupportExt.AttributeMappingBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 * = PlatoRepositoryImpl
 *
 * TODO Auto-generated class documentation
 *
 */
@RooJpaRepositoryCustomImpl(repository = PlatoRepositoryCustom.class)
@Transactional(readOnly = true)
public class PlatoRepositoryImpl extends QueryDslRepositorySupportExt<Plato> implements PlatoRepositoryCustom {

    /**
     * TODO Auto-generated constructor documentation
     */
    PlatoRepositoryImpl() {
        super(Plato.class);
    }

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    public static final String RESTAURANTE = "restaurante";

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    public static final String DESCRIPCION = "descripcion";

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    public static final String NOMBRE = "nombre";

    /**
     * TODO Auto-generated method documentation
     *
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<Plato> findAll(GlobalSearch globalSearch, Pageable pageable) {
        QPlato plato = QPlato.plato;
        JPQLQuery<Plato> query = from(plato);
        Path<?>[] paths = new Path<?>[] { plato.restaurante, plato.nombre, plato.descripcion };
        applyGlobalSearch(globalSearch, query, paths);
        AttributeMappingBuilder mapping = buildMapper().map(RESTAURANTE, plato.restaurante).map(NOMBRE, plato.nombre).map(DESCRIPCION, plato.descripcion);
        applyPagination(pageable, query, mapping);
        applyOrderById(query);
        return loadPage(query, pageable, plato);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param restaurante
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<Plato> findByRestaurante(Restaurante restaurante, GlobalSearch globalSearch, Pageable pageable) {
        QPlato plato = QPlato.plato;
        JPQLQuery<Plato> query = from(plato);
        Assert.notNull(restaurante, "restaurante is required");
        query.where(plato.restaurante.eq(restaurante));
        Path<?>[] paths = new Path<?>[] { plato.restaurante, plato.nombre, plato.descripcion };
        applyGlobalSearch(globalSearch, query, paths);
        AttributeMappingBuilder mapping = buildMapper().map(RESTAURANTE, plato.restaurante).map(NOMBRE, plato.nombre).map(DESCRIPCION, plato.descripcion);
        applyPagination(pageable, query, mapping);
        applyOrderById(query);
        return loadPage(query, pageable, plato);
    }
}
