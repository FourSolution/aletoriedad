package com.aletoriedad.repository;
import com.aletoriedad.model.Restaurante;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepository;
import io.springlets.data.jpa.repository.DetachableJpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * = RestauranteRepository
 *
 * TODO Auto-generated class documentation
 *
 */
@RooJpaRepository(entity = Restaurante.class)
@Transactional(readOnly = true)
public interface RestauranteRepository extends DetachableJpaRepository<Restaurante, Long>, RestauranteRepositoryCustom {
}
