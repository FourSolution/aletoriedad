package com.aletoriedad.repository;
import io.springlets.data.jpa.repository.support.QueryDslRepositorySupportExt;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepositoryCustomImpl;
import com.aletoriedad.model.Restaurante;
import com.aletoriedad.model.QRestaurante;
import com.querydsl.core.types.Path;
import com.querydsl.jpa.JPQLQuery;
import io.springlets.data.domain.GlobalSearch;
import io.springlets.data.jpa.repository.support.QueryDslRepositorySupportExt.AttributeMappingBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

/**
 * = RestauranteRepositoryImpl
 *
 * TODO Auto-generated class documentation
 *
 */
@RooJpaRepositoryCustomImpl(repository = RestauranteRepositoryCustom.class)
@Transactional(readOnly = true)
public class RestauranteRepositoryImpl extends QueryDslRepositorySupportExt<Restaurante> implements RestauranteRepositoryCustom {

    /**
     * TODO Auto-generated constructor documentation
     */
    RestauranteRepositoryImpl() {
        super(Restaurante.class);
    }

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    public static final String DIRECCION = "direccion";

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    public static final String DESCRIPCION = "descripcion";

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    public static final String NOMBRE = "nombre";

    /**
     * TODO Auto-generated method documentation
     *
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<Restaurante> findAll(GlobalSearch globalSearch, Pageable pageable) {
        QRestaurante restaurante = QRestaurante.restaurante;
        JPQLQuery<Restaurante> query = from(restaurante);
        Path<?>[] paths = new Path<?>[] { restaurante.nombre, restaurante.direccion, restaurante.descripcion };
        applyGlobalSearch(globalSearch, query, paths);
        AttributeMappingBuilder mapping = buildMapper().map(NOMBRE, restaurante.nombre).map(DIRECCION, restaurante.direccion).map(DESCRIPCION, restaurante.descripcion);
        applyPagination(pageable, query, mapping);
        applyOrderById(query);
        return loadPage(query, pageable, restaurante);
    }
}
