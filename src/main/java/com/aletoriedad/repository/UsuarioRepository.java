package com.aletoriedad.repository;
import com.aletoriedad.model.Usuario;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepository;
import com.aletoriedad.model.Persona;
import io.springlets.data.jpa.repository.DetachableJpaRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * = UsuarioRepository
 *
 * TODO Auto-generated class documentation
 *
 */
@RooJpaRepository(entity = Usuario.class)
@Transactional(readOnly = true)
public interface UsuarioRepository extends DetachableJpaRepository<Usuario, Long>, UsuarioRepositoryCustom {

    /**
     * TODO Auto-generated method documentation
     *
     * @param persona
     * @return Long
     */
    public abstract long countByPersona(Persona persona);
}
