package com.aletoriedad.repository;
import com.aletoriedad.model.Usuario;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepositoryCustom;
import com.aletoriedad.model.Persona;
import io.springlets.data.domain.GlobalSearch;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * = UsuarioRepositoryCustom
 *
 * TODO Auto-generated class documentation
 *
 */
@RooJpaRepositoryCustom(entity = Usuario.class)
public interface UsuarioRepositoryCustom {

    /**
     * TODO Auto-generated method documentation
     *
     * @param persona
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public abstract Page<Usuario> findByPersona(Persona persona, GlobalSearch globalSearch, Pageable pageable);

    /**
     * TODO Auto-generated method documentation
     *
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public abstract Page<Usuario> findAll(GlobalSearch globalSearch, Pageable pageable);
}
