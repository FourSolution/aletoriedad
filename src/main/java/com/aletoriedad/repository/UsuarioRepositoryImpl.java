package com.aletoriedad.repository;
import io.springlets.data.jpa.repository.support.QueryDslRepositorySupportExt;
import org.springframework.roo.addon.layers.repository.jpa.annotations.RooJpaRepositoryCustomImpl;
import com.aletoriedad.model.Usuario;
import com.aletoriedad.model.Persona;
import com.aletoriedad.model.QUsuario;
import com.querydsl.core.types.Path;
import com.querydsl.jpa.JPQLQuery;
import io.springlets.data.domain.GlobalSearch;
import io.springlets.data.jpa.repository.support.QueryDslRepositorySupportExt.AttributeMappingBuilder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 * = UsuarioRepositoryImpl
 *
 * TODO Auto-generated class documentation
 *
 */
@RooJpaRepositoryCustomImpl(repository = UsuarioRepositoryCustom.class)
@Transactional(readOnly = true)
public class UsuarioRepositoryImpl extends QueryDslRepositorySupportExt<Usuario> implements UsuarioRepositoryCustom {

    /**
     * TODO Auto-generated constructor documentation
     */
    UsuarioRepositoryImpl() {
        super(Usuario.class);
    }

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    public static final String PASSWORD = "password";

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    public static final String PERSONA = "persona";

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    public static final String EMAIL = "email";

    /**
     * TODO Auto-generated method documentation
     *
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<Usuario> findAll(GlobalSearch globalSearch, Pageable pageable) {
        QUsuario usuario = QUsuario.usuario;
        JPQLQuery<Usuario> query = from(usuario);
        Path<?>[] paths = new Path<?>[] { usuario.persona, usuario.email, usuario.password };
        applyGlobalSearch(globalSearch, query, paths);
        AttributeMappingBuilder mapping = buildMapper().map(PERSONA, usuario.persona).map(EMAIL, usuario.email).map(PASSWORD, usuario.password);
        applyPagination(pageable, query, mapping);
        applyOrderById(query);
        return loadPage(query, pageable, usuario);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param persona
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<Usuario> findByPersona(Persona persona, GlobalSearch globalSearch, Pageable pageable) {
        QUsuario usuario = QUsuario.usuario;
        JPQLQuery<Usuario> query = from(usuario);
        Assert.notNull(persona, "persona is required");
        query.where(usuario.persona.eq(persona));
        Path<?>[] paths = new Path<?>[] { usuario.persona, usuario.email, usuario.password };
        applyGlobalSearch(globalSearch, query, paths);
        AttributeMappingBuilder mapping = buildMapper().map(PERSONA, usuario.persona).map(EMAIL, usuario.email).map(PASSWORD, usuario.password);
        applyPagination(pageable, query, mapping);
        applyOrderById(query);
        return loadPage(query, pageable, usuario);
    }
}
