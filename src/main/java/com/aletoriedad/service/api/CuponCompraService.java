package com.aletoriedad.service.api;
import com.aletoriedad.model.CuponCompra;
import org.springframework.roo.addon.layers.service.annotations.RooService;
import com.aletoriedad.model.Plato;
import com.aletoriedad.model.Usuario;
import io.springlets.data.domain.GlobalSearch;
import io.springlets.format.EntityResolver;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * = CuponCompraService
 *
 * TODO Auto-generated class documentation
 *
 */
@RooService(entity = CuponCompra.class)
public interface CuponCompraService extends EntityResolver<CuponCompra, Long> {

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @return CuponCompra
     */
    public abstract CuponCompra findOne(Long id);

    /**
     * TODO Auto-generated method documentation
     *
     * @param cuponCompra
     */
    public abstract void delete(CuponCompra cuponCompra);

    /**
     * TODO Auto-generated method documentation
     *
     * @param entities
     * @return List
     */
    public abstract List<CuponCompra> save(Iterable<CuponCompra> entities);

    /**
     * TODO Auto-generated method documentation
     *
     * @param ids
     */
    public abstract void delete(Iterable<Long> ids);

    /**
     * TODO Auto-generated method documentation
     *
     * @param entity
     * @return CuponCompra
     */
    public abstract CuponCompra save(CuponCompra entity);

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @return CuponCompra
     */
    public abstract CuponCompra findOneForUpdate(Long id);

    /**
     * TODO Auto-generated method documentation
     *
     * @param ids
     * @return List
     */
    public abstract List<CuponCompra> findAll(Iterable<Long> ids);

    /**
     * TODO Auto-generated method documentation
     *
     * @return List
     */
    public abstract List<CuponCompra> findAll();

    /**
     * TODO Auto-generated method documentation
     *
     * @return Long
     */
    public abstract long count();

    /**
     * TODO Auto-generated method documentation
     *
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public abstract Page<CuponCompra> findAll(GlobalSearch globalSearch, Pageable pageable);

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public abstract Page<CuponCompra> findByPlato(Plato plato, GlobalSearch globalSearch, Pageable pageable);

    /**
     * TODO Auto-generated method documentation
     *
     * @param usuario
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public abstract Page<CuponCompra> findByUsuario(Usuario usuario, GlobalSearch globalSearch, Pageable pageable);

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @return Long
     */
    public abstract long countByPlato(Plato plato);

    /**
     * TODO Auto-generated method documentation
     *
     * @param usuario
     * @return Long
     */
    public abstract long countByUsuario(Usuario usuario);
}
