package com.aletoriedad.service.api;
import com.aletoriedad.model.IngredientePlato;
import org.springframework.roo.addon.layers.service.annotations.RooService;
import com.aletoriedad.model.Ingrediente;
import com.aletoriedad.model.Plato;
import io.springlets.data.domain.GlobalSearch;
import io.springlets.format.EntityResolver;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * = IngredientePlatoService
 *
 * TODO Auto-generated class documentation
 *
 */
@RooService(entity = IngredientePlato.class)
public interface IngredientePlatoService extends EntityResolver<IngredientePlato, Long> {

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @return IngredientePlato
     */
    public abstract IngredientePlato findOne(Long id);

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingredientePlato
     */
    public abstract void delete(IngredientePlato ingredientePlato);

    /**
     * TODO Auto-generated method documentation
     *
     * @param entities
     * @return List
     */
    public abstract List<IngredientePlato> save(Iterable<IngredientePlato> entities);

    /**
     * TODO Auto-generated method documentation
     *
     * @param ids
     */
    public abstract void delete(Iterable<Long> ids);

    /**
     * TODO Auto-generated method documentation
     *
     * @param entity
     * @return IngredientePlato
     */
    public abstract IngredientePlato save(IngredientePlato entity);

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @return IngredientePlato
     */
    public abstract IngredientePlato findOneForUpdate(Long id);

    /**
     * TODO Auto-generated method documentation
     *
     * @param ids
     * @return List
     */
    public abstract List<IngredientePlato> findAll(Iterable<Long> ids);

    /**
     * TODO Auto-generated method documentation
     *
     * @return List
     */
    public abstract List<IngredientePlato> findAll();

    /**
     * TODO Auto-generated method documentation
     *
     * @return Long
     */
    public abstract long count();

    /**
     * TODO Auto-generated method documentation
     *
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public abstract Page<IngredientePlato> findAll(GlobalSearch globalSearch, Pageable pageable);

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingrediente
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public abstract Page<IngredientePlato> findByIngrediente(Ingrediente ingrediente, GlobalSearch globalSearch, Pageable pageable);

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public abstract Page<IngredientePlato> findByPlato(Plato plato, GlobalSearch globalSearch, Pageable pageable);

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingrediente
     * @return Long
     */
    public abstract long countByIngrediente(Ingrediente ingrediente);

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @return Long
     */
    public abstract long countByPlato(Plato plato);
}
