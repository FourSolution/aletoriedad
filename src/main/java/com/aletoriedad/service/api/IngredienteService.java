package com.aletoriedad.service.api;
import com.aletoriedad.model.Ingrediente;
import org.springframework.roo.addon.layers.service.annotations.RooService;
import io.springlets.data.domain.GlobalSearch;
import io.springlets.format.EntityResolver;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * = IngredienteService
 *
 * TODO Auto-generated class documentation
 *
 */
@RooService(entity = Ingrediente.class)
public interface IngredienteService extends EntityResolver<Ingrediente, Long> {

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @return Ingrediente
     */
    public abstract Ingrediente findOne(Long id);

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingrediente
     */
    public abstract void delete(Ingrediente ingrediente);

    /**
     * TODO Auto-generated method documentation
     *
     * @param entities
     * @return List
     */
    public abstract List<Ingrediente> save(Iterable<Ingrediente> entities);

    /**
     * TODO Auto-generated method documentation
     *
     * @param ids
     */
    public abstract void delete(Iterable<Long> ids);

    /**
     * TODO Auto-generated method documentation
     *
     * @param entity
     * @return Ingrediente
     */
    public abstract Ingrediente save(Ingrediente entity);

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @return Ingrediente
     */
    public abstract Ingrediente findOneForUpdate(Long id);

    /**
     * TODO Auto-generated method documentation
     *
     * @param ids
     * @return List
     */
    public abstract List<Ingrediente> findAll(Iterable<Long> ids);

    /**
     * TODO Auto-generated method documentation
     *
     * @return List
     */
    public abstract List<Ingrediente> findAll();

    /**
     * TODO Auto-generated method documentation
     *
     * @return Long
     */
    public abstract long count();

    /**
     * TODO Auto-generated method documentation
     *
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public abstract Page<Ingrediente> findAll(GlobalSearch globalSearch, Pageable pageable);

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingrediente
     * @param platosingredientesToAdd
     * @return Ingrediente
     */
    public abstract Ingrediente addToPlatosingredientes(Ingrediente ingrediente, Iterable<Long> platosingredientesToAdd);

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingrediente
     * @param platosingredientesToRemove
     * @return Ingrediente
     */
    public abstract Ingrediente removeFromPlatosingredientes(Ingrediente ingrediente, Iterable<Long> platosingredientesToRemove);

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingrediente
     * @param platosingredientes
     * @return Ingrediente
     */
    public abstract Ingrediente setPlatosingredientes(Ingrediente ingrediente, Iterable<Long> platosingredientes);
}
