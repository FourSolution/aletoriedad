package com.aletoriedad.service.api;
import com.aletoriedad.model.Persona;
import org.springframework.roo.addon.layers.service.annotations.RooService;
import io.springlets.data.domain.GlobalSearch;
import io.springlets.format.EntityResolver;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * = PersonaService
 *
 * TODO Auto-generated class documentation
 *
 */
@RooService(entity = Persona.class)
public interface PersonaService extends EntityResolver<Persona, Long> {

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @return Persona
     */
    public abstract Persona findOne(Long id);

    /**
     * TODO Auto-generated method documentation
     *
     * @param persona
     */
    public abstract void delete(Persona persona);

    /**
     * TODO Auto-generated method documentation
     *
     * @param entities
     * @return List
     */
    public abstract List<Persona> save(Iterable<Persona> entities);

    /**
     * TODO Auto-generated method documentation
     *
     * @param ids
     */
    public abstract void delete(Iterable<Long> ids);

    /**
     * TODO Auto-generated method documentation
     *
     * @param entity
     * @return Persona
     */
    public abstract Persona save(Persona entity);

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @return Persona
     */
    public abstract Persona findOneForUpdate(Long id);

    /**
     * TODO Auto-generated method documentation
     *
     * @param ids
     * @return List
     */
    public abstract List<Persona> findAll(Iterable<Long> ids);

    /**
     * TODO Auto-generated method documentation
     *
     * @return List
     */
    public abstract List<Persona> findAll();

    /**
     * TODO Auto-generated method documentation
     *
     * @return Long
     */
    public abstract long count();

    /**
     * TODO Auto-generated method documentation
     *
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public abstract Page<Persona> findAll(GlobalSearch globalSearch, Pageable pageable);

    /**
     * TODO Auto-generated method documentation
     *
     * @param persona
     * @param usuariosToAdd
     * @return Persona
     */
    public abstract Persona addToUsuarios(Persona persona, Iterable<Long> usuariosToAdd);

    /**
     * TODO Auto-generated method documentation
     *
     * @param persona
     * @param usuariosToRemove
     * @return Persona
     */
    public abstract Persona removeFromUsuarios(Persona persona, Iterable<Long> usuariosToRemove);

    /**
     * TODO Auto-generated method documentation
     *
     * @param persona
     * @param usuarios
     * @return Persona
     */
    public abstract Persona setUsuarios(Persona persona, Iterable<Long> usuarios);
}
