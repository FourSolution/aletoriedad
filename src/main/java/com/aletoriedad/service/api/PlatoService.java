package com.aletoriedad.service.api;
import com.aletoriedad.model.Plato;
import org.springframework.roo.addon.layers.service.annotations.RooService;
import com.aletoriedad.model.Restaurante;
import io.springlets.data.domain.GlobalSearch;
import io.springlets.format.EntityResolver;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * = PlatoService
 *
 * TODO Auto-generated class documentation
 *
 */
@RooService(entity = Plato.class)
public interface PlatoService extends EntityResolver<Plato, Long> {

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @return Plato
     */
    public abstract Plato findOne(Long id);

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     */
    public abstract void delete(Plato plato);

    /**
     * TODO Auto-generated method documentation
     *
     * @param entities
     * @return List
     */
    public abstract List<Plato> save(Iterable<Plato> entities);

    /**
     * TODO Auto-generated method documentation
     *
     * @param ids
     */
    public abstract void delete(Iterable<Long> ids);

    /**
     * TODO Auto-generated method documentation
     *
     * @param entity
     * @return Plato
     */
    public abstract Plato save(Plato entity);

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @return Plato
     */
    public abstract Plato findOneForUpdate(Long id);

    /**
     * TODO Auto-generated method documentation
     *
     * @param ids
     * @return List
     */
    public abstract List<Plato> findAll(Iterable<Long> ids);

    /**
     * TODO Auto-generated method documentation
     *
     * @return List
     */
    public abstract List<Plato> findAll();

    /**
     * TODO Auto-generated method documentation
     *
     * @return Long
     */
    public abstract long count();

    /**
     * TODO Auto-generated method documentation
     *
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public abstract Page<Plato> findAll(GlobalSearch globalSearch, Pageable pageable);

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @param ingredientesplatosToAdd
     * @return Plato
     */
    public abstract Plato addToIngredientesplatos(Plato plato, Iterable<Long> ingredientesplatosToAdd);

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @param ingredientesplatosToRemove
     * @return Plato
     */
    public abstract Plato removeFromIngredientesplatos(Plato plato, Iterable<Long> ingredientesplatosToRemove);

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @param ingredientesplatos
     * @return Plato
     */
    public abstract Plato setIngredientesplatos(Plato plato, Iterable<Long> ingredientesplatos);

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @param platocuponToAdd
     * @return Plato
     */
    public abstract Plato addToPlatocupon(Plato plato, Iterable<Long> platocuponToAdd);

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @param platocuponToRemove
     * @return Plato
     */
    public abstract Plato removeFromPlatocupon(Plato plato, Iterable<Long> platocuponToRemove);

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @param platocupon
     * @return Plato
     */
    public abstract Plato setPlatocupon(Plato plato, Iterable<Long> platocupon);

    /**
     * TODO Auto-generated method documentation
     *
     * @param restaurante
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public abstract Page<Plato> findByRestaurante(Restaurante restaurante, GlobalSearch globalSearch, Pageable pageable);

    /**
     * TODO Auto-generated method documentation
     *
     * @param restaurante
     * @return Long
     */
    public abstract long countByRestaurante(Restaurante restaurante);
}
