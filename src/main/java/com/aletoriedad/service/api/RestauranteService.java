package com.aletoriedad.service.api;
import com.aletoriedad.model.Restaurante;
import org.springframework.roo.addon.layers.service.annotations.RooService;
import io.springlets.data.domain.GlobalSearch;
import io.springlets.format.EntityResolver;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * = RestauranteService
 *
 * TODO Auto-generated class documentation
 *
 */
@RooService(entity = Restaurante.class)
public interface RestauranteService extends EntityResolver<Restaurante, Long> {

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @return Restaurante
     */
    public abstract Restaurante findOne(Long id);

    /**
     * TODO Auto-generated method documentation
     *
     * @param restaurante
     */
    public abstract void delete(Restaurante restaurante);

    /**
     * TODO Auto-generated method documentation
     *
     * @param entities
     * @return List
     */
    public abstract List<Restaurante> save(Iterable<Restaurante> entities);

    /**
     * TODO Auto-generated method documentation
     *
     * @param ids
     */
    public abstract void delete(Iterable<Long> ids);

    /**
     * TODO Auto-generated method documentation
     *
     * @param entity
     * @return Restaurante
     */
    public abstract Restaurante save(Restaurante entity);

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @return Restaurante
     */
    public abstract Restaurante findOneForUpdate(Long id);

    /**
     * TODO Auto-generated method documentation
     *
     * @param ids
     * @return List
     */
    public abstract List<Restaurante> findAll(Iterable<Long> ids);

    /**
     * TODO Auto-generated method documentation
     *
     * @return List
     */
    public abstract List<Restaurante> findAll();

    /**
     * TODO Auto-generated method documentation
     *
     * @return Long
     */
    public abstract long count();

    /**
     * TODO Auto-generated method documentation
     *
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public abstract Page<Restaurante> findAll(GlobalSearch globalSearch, Pageable pageable);

    /**
     * TODO Auto-generated method documentation
     *
     * @param restaurante
     * @param platosToAdd
     * @return Restaurante
     */
    public abstract Restaurante addToPlatos(Restaurante restaurante, Iterable<Long> platosToAdd);

    /**
     * TODO Auto-generated method documentation
     *
     * @param restaurante
     * @param platosToRemove
     * @return Restaurante
     */
    public abstract Restaurante removeFromPlatos(Restaurante restaurante, Iterable<Long> platosToRemove);

    /**
     * TODO Auto-generated method documentation
     *
     * @param restaurante
     * @param platos
     * @return Restaurante
     */
    public abstract Restaurante setPlatos(Restaurante restaurante, Iterable<Long> platos);
}
