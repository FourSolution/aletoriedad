package com.aletoriedad.service.api;
import com.aletoriedad.model.Usuario;
import org.springframework.roo.addon.layers.service.annotations.RooService;
import com.aletoriedad.model.Persona;
import io.springlets.data.domain.GlobalSearch;
import io.springlets.format.EntityResolver;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * = UsuarioService
 *
 * TODO Auto-generated class documentation
 *
 */
@RooService(entity = Usuario.class)
public interface UsuarioService extends EntityResolver<Usuario, Long> {

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @return Usuario
     */
    public abstract Usuario findOne(Long id);

    /**
     * TODO Auto-generated method documentation
     *
     * @param usuario
     */
    public abstract void delete(Usuario usuario);

    /**
     * TODO Auto-generated method documentation
     *
     * @param entities
     * @return List
     */
    public abstract List<Usuario> save(Iterable<Usuario> entities);

    /**
     * TODO Auto-generated method documentation
     *
     * @param ids
     */
    public abstract void delete(Iterable<Long> ids);

    /**
     * TODO Auto-generated method documentation
     *
     * @param entity
     * @return Usuario
     */
    public abstract Usuario save(Usuario entity);

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @return Usuario
     */
    public abstract Usuario findOneForUpdate(Long id);

    /**
     * TODO Auto-generated method documentation
     *
     * @param ids
     * @return List
     */
    public abstract List<Usuario> findAll(Iterable<Long> ids);

    /**
     * TODO Auto-generated method documentation
     *
     * @return List
     */
    public abstract List<Usuario> findAll();

    /**
     * TODO Auto-generated method documentation
     *
     * @return Long
     */
    public abstract long count();

    /**
     * TODO Auto-generated method documentation
     *
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public abstract Page<Usuario> findAll(GlobalSearch globalSearch, Pageable pageable);

    /**
     * TODO Auto-generated method documentation
     *
     * @param usuario
     * @param usuariocuponToAdd
     * @return Usuario
     */
    public abstract Usuario addToUsuariocupon(Usuario usuario, Iterable<Long> usuariocuponToAdd);

    /**
     * TODO Auto-generated method documentation
     *
     * @param usuario
     * @param usuariocuponToRemove
     * @return Usuario
     */
    public abstract Usuario removeFromUsuariocupon(Usuario usuario, Iterable<Long> usuariocuponToRemove);

    /**
     * TODO Auto-generated method documentation
     *
     * @param usuario
     * @param usuariocupon
     * @return Usuario
     */
    public abstract Usuario setUsuariocupon(Usuario usuario, Iterable<Long> usuariocupon);

    /**
     * TODO Auto-generated method documentation
     *
     * @param persona
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public abstract Page<Usuario> findByPersona(Persona persona, GlobalSearch globalSearch, Pageable pageable);

    /**
     * TODO Auto-generated method documentation
     *
     * @param persona
     * @return Long
     */
    public abstract long countByPersona(Persona persona);
}
