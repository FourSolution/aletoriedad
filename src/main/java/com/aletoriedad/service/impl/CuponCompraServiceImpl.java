package com.aletoriedad.service.impl;
import com.aletoriedad.service.api.CuponCompraService;
import org.springframework.roo.addon.layers.service.annotations.RooServiceImpl;
import com.aletoriedad.model.CuponCompra;
import com.aletoriedad.model.Plato;
import com.aletoriedad.model.Usuario;
import com.aletoriedad.repository.CuponCompraRepository;
import io.springlets.data.domain.GlobalSearch;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * = CuponCompraServiceImpl
 *
 * TODO Auto-generated class documentation
 *
 */
@RooServiceImpl(service = CuponCompraService.class)
@Service
@Transactional(readOnly = true)
public class CuponCompraServiceImpl implements CuponCompraService {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private CuponCompraRepository cuponCompraRepository;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param cuponCompraRepository
     */
    @Autowired
    public CuponCompraServiceImpl(CuponCompraRepository cuponCompraRepository) {
        setCuponCompraRepository(cuponCompraRepository);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return CuponCompraRepository
     */
    public CuponCompraRepository getCuponCompraRepository() {
        return cuponCompraRepository;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param cuponCompraRepository
     */
    public void setCuponCompraRepository(CuponCompraRepository cuponCompraRepository) {
        this.cuponCompraRepository = cuponCompraRepository;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param cuponCompra
     */
    @Transactional
    public void delete(CuponCompra cuponCompra) {
        // Clear bidirectional many-to-one child relationship with Usuario
        if (cuponCompra.getUsuario() != null) {
            cuponCompra.getUsuario().getUsuariocupon().remove(cuponCompra);
        }
        // Clear bidirectional many-to-one child relationship with Plato
        if (cuponCompra.getPlato() != null) {
            cuponCompra.getPlato().getPlatocupon().remove(cuponCompra);
        }
        getCuponCompraRepository().delete(cuponCompra);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param entities
     * @return List
     */
    @Transactional
    public List<CuponCompra> save(Iterable<CuponCompra> entities) {
        return getCuponCompraRepository().save(entities);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ids
     */
    @Transactional
    public void delete(Iterable<Long> ids) {
        List<CuponCompra> toDelete = getCuponCompraRepository().findAll(ids);
        getCuponCompraRepository().deleteInBatch(toDelete);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param entity
     * @return CuponCompra
     */
    @Transactional
    public CuponCompra save(CuponCompra entity) {
        return getCuponCompraRepository().save(entity);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @return CuponCompra
     */
    public CuponCompra findOne(Long id) {
        return getCuponCompraRepository().findOne(id);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @return CuponCompra
     */
    public CuponCompra findOneForUpdate(Long id) {
        return getCuponCompraRepository().findOneDetached(id);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ids
     * @return List
     */
    public List<CuponCompra> findAll(Iterable<Long> ids) {
        return getCuponCompraRepository().findAll(ids);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return List
     */
    public List<CuponCompra> findAll() {
        return getCuponCompraRepository().findAll();
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Long
     */
    public long count() {
        return getCuponCompraRepository().count();
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<CuponCompra> findAll(GlobalSearch globalSearch, Pageable pageable) {
        return getCuponCompraRepository().findAll(globalSearch, pageable);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<CuponCompra> findByPlato(Plato plato, GlobalSearch globalSearch, Pageable pageable) {
        return getCuponCompraRepository().findByPlato(plato, globalSearch, pageable);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param usuario
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<CuponCompra> findByUsuario(Usuario usuario, GlobalSearch globalSearch, Pageable pageable) {
        return getCuponCompraRepository().findByUsuario(usuario, globalSearch, pageable);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @return Long
     */
    public long countByPlato(Plato plato) {
        return getCuponCompraRepository().countByPlato(plato);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param usuario
     * @return Long
     */
    public long countByUsuario(Usuario usuario) {
        return getCuponCompraRepository().countByUsuario(usuario);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Class
     */
    public Class<CuponCompra> getEntityType() {
        return CuponCompra.class;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Class
     */
    public Class<Long> getIdType() {
        return Long.class;
    }
}
