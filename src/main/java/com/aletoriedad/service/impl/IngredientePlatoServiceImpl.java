package com.aletoriedad.service.impl;
import com.aletoriedad.service.api.IngredientePlatoService;
import org.springframework.roo.addon.layers.service.annotations.RooServiceImpl;
import com.aletoriedad.model.Ingrediente;
import com.aletoriedad.model.IngredientePlato;
import com.aletoriedad.model.Plato;
import com.aletoriedad.repository.IngredientePlatoRepository;
import io.springlets.data.domain.GlobalSearch;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * = IngredientePlatoServiceImpl
 *
 * TODO Auto-generated class documentation
 *
 */
@RooServiceImpl(service = IngredientePlatoService.class)
@Service
@Transactional(readOnly = true)
public class IngredientePlatoServiceImpl implements IngredientePlatoService {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private IngredientePlatoRepository ingredientePlatoRepository;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param ingredientePlatoRepository
     */
    @Autowired
    public IngredientePlatoServiceImpl(IngredientePlatoRepository ingredientePlatoRepository) {
        setIngredientePlatoRepository(ingredientePlatoRepository);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return IngredientePlatoRepository
     */
    public IngredientePlatoRepository getIngredientePlatoRepository() {
        return ingredientePlatoRepository;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingredientePlatoRepository
     */
    public void setIngredientePlatoRepository(IngredientePlatoRepository ingredientePlatoRepository) {
        this.ingredientePlatoRepository = ingredientePlatoRepository;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingredientePlato
     */
    @Transactional
    public void delete(IngredientePlato ingredientePlato) {
        // Clear bidirectional many-to-one child relationship with Plato
        if (ingredientePlato.getPlato() != null) {
            ingredientePlato.getPlato().getIngredientesplatos().remove(ingredientePlato);
        }
        // Clear bidirectional many-to-one child relationship with Ingrediente
        if (ingredientePlato.getIngrediente() != null) {
            ingredientePlato.getIngrediente().getPlatosingredientes().remove(ingredientePlato);
        }
        getIngredientePlatoRepository().delete(ingredientePlato);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param entities
     * @return List
     */
    @Transactional
    public List<IngredientePlato> save(Iterable<IngredientePlato> entities) {
        return getIngredientePlatoRepository().save(entities);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ids
     */
    @Transactional
    public void delete(Iterable<Long> ids) {
        List<IngredientePlato> toDelete = getIngredientePlatoRepository().findAll(ids);
        getIngredientePlatoRepository().deleteInBatch(toDelete);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param entity
     * @return IngredientePlato
     */
    @Transactional
    public IngredientePlato save(IngredientePlato entity) {
        return getIngredientePlatoRepository().save(entity);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @return IngredientePlato
     */
    public IngredientePlato findOne(Long id) {
        return getIngredientePlatoRepository().findOne(id);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @return IngredientePlato
     */
    public IngredientePlato findOneForUpdate(Long id) {
        return getIngredientePlatoRepository().findOneDetached(id);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ids
     * @return List
     */
    public List<IngredientePlato> findAll(Iterable<Long> ids) {
        return getIngredientePlatoRepository().findAll(ids);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return List
     */
    public List<IngredientePlato> findAll() {
        return getIngredientePlatoRepository().findAll();
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Long
     */
    public long count() {
        return getIngredientePlatoRepository().count();
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<IngredientePlato> findAll(GlobalSearch globalSearch, Pageable pageable) {
        return getIngredientePlatoRepository().findAll(globalSearch, pageable);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingrediente
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<IngredientePlato> findByIngrediente(Ingrediente ingrediente, GlobalSearch globalSearch, Pageable pageable) {
        return getIngredientePlatoRepository().findByIngrediente(ingrediente, globalSearch, pageable);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<IngredientePlato> findByPlato(Plato plato, GlobalSearch globalSearch, Pageable pageable) {
        return getIngredientePlatoRepository().findByPlato(plato, globalSearch, pageable);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingrediente
     * @return Long
     */
    public long countByIngrediente(Ingrediente ingrediente) {
        return getIngredientePlatoRepository().countByIngrediente(ingrediente);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @return Long
     */
    public long countByPlato(Plato plato) {
        return getIngredientePlatoRepository().countByPlato(plato);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Class
     */
    public Class<IngredientePlato> getEntityType() {
        return IngredientePlato.class;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Class
     */
    public Class<Long> getIdType() {
        return Long.class;
    }
}
