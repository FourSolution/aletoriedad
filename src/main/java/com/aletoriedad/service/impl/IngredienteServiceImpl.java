package com.aletoriedad.service.impl;
import com.aletoriedad.service.api.IngredienteService;
import org.springframework.roo.addon.layers.service.annotations.RooServiceImpl;
import com.aletoriedad.model.Ingrediente;
import com.aletoriedad.model.IngredientePlato;
import com.aletoriedad.repository.IngredienteRepository;
import com.aletoriedad.service.api.IngredientePlatoService;
import io.springlets.data.domain.GlobalSearch;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * = IngredienteServiceImpl
 *
 * TODO Auto-generated class documentation
 *
 */
@RooServiceImpl(service = IngredienteService.class)
@Service
@Transactional(readOnly = true)
public class IngredienteServiceImpl implements IngredienteService {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private IngredienteRepository ingredienteRepository;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private IngredientePlatoService ingredientePlatoService;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param ingredienteRepository
     * @param ingredientePlatoService
     */
    @Autowired
    public IngredienteServiceImpl(IngredienteRepository ingredienteRepository, @Lazy IngredientePlatoService ingredientePlatoService) {
        setIngredienteRepository(ingredienteRepository);
        setIngredientePlatoService(ingredientePlatoService);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return IngredienteRepository
     */
    public IngredienteRepository getIngredienteRepository() {
        return ingredienteRepository;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingredienteRepository
     */
    public void setIngredienteRepository(IngredienteRepository ingredienteRepository) {
        this.ingredienteRepository = ingredienteRepository;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return IngredientePlatoService
     */
    public IngredientePlatoService getIngredientePlatoService() {
        return ingredientePlatoService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingredientePlatoService
     */
    public void setIngredientePlatoService(IngredientePlatoService ingredientePlatoService) {
        this.ingredientePlatoService = ingredientePlatoService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingrediente
     * @param platosingredientesToAdd
     * @return Ingrediente
     */
    @Transactional
    public Ingrediente addToPlatosingredientes(Ingrediente ingrediente, Iterable<Long> platosingredientesToAdd) {
        List<IngredientePlato> platosingredientes = getIngredientePlatoService().findAll(platosingredientesToAdd);
        ingrediente.addToPlatosingredientes(platosingredientes);
        return getIngredienteRepository().save(ingrediente);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingrediente
     * @param platosingredientesToRemove
     * @return Ingrediente
     */
    @Transactional
    public Ingrediente removeFromPlatosingredientes(Ingrediente ingrediente, Iterable<Long> platosingredientesToRemove) {
        List<IngredientePlato> platosingredientes = getIngredientePlatoService().findAll(platosingredientesToRemove);
        ingrediente.removeFromPlatosingredientes(platosingredientes);
        return getIngredienteRepository().save(ingrediente);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingrediente
     * @param platosingredientes
     * @return Ingrediente
     */
    @Transactional
    public Ingrediente setPlatosingredientes(Ingrediente ingrediente, Iterable<Long> platosingredientes) {
        List<IngredientePlato> items = getIngredientePlatoService().findAll(platosingredientes);
        Set<IngredientePlato> currents = ingrediente.getPlatosingredientes();
        Set<IngredientePlato> toRemove = new HashSet<IngredientePlato>();
        for (Iterator<IngredientePlato> iterator = currents.iterator(); iterator.hasNext(); ) {
            IngredientePlato nextIngredientePlato = iterator.next();
            if (items.contains(nextIngredientePlato)) {
                items.remove(nextIngredientePlato);
            } else {
                toRemove.add(nextIngredientePlato);
            }
        }
        ingrediente.removeFromPlatosingredientes(toRemove);
        ingrediente.addToPlatosingredientes(items);
        // Force the version update of the parent side to know that the parent has changed
        // because it has new books assigned
        ingrediente.setVersion(ingrediente.getVersion() + 1);
        return getIngredienteRepository().save(ingrediente);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingrediente
     */
    @Transactional
    public void delete(Ingrediente ingrediente) {
        // Clear bidirectional one-to-many parent relationship with IngredientePlato
        for (IngredientePlato item : ingrediente.getPlatosingredientes()) {
            item.setIngrediente(null);
        }
        getIngredienteRepository().delete(ingrediente);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param entities
     * @return List
     */
    @Transactional
    public List<Ingrediente> save(Iterable<Ingrediente> entities) {
        return getIngredienteRepository().save(entities);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ids
     */
    @Transactional
    public void delete(Iterable<Long> ids) {
        List<Ingrediente> toDelete = getIngredienteRepository().findAll(ids);
        getIngredienteRepository().deleteInBatch(toDelete);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param entity
     * @return Ingrediente
     */
    @Transactional
    public Ingrediente save(Ingrediente entity) {
        return getIngredienteRepository().save(entity);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @return Ingrediente
     */
    public Ingrediente findOne(Long id) {
        return getIngredienteRepository().findOne(id);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @return Ingrediente
     */
    public Ingrediente findOneForUpdate(Long id) {
        return getIngredienteRepository().findOneDetached(id);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ids
     * @return List
     */
    public List<Ingrediente> findAll(Iterable<Long> ids) {
        return getIngredienteRepository().findAll(ids);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return List
     */
    public List<Ingrediente> findAll() {
        return getIngredienteRepository().findAll();
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Long
     */
    public long count() {
        return getIngredienteRepository().count();
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<Ingrediente> findAll(GlobalSearch globalSearch, Pageable pageable) {
        return getIngredienteRepository().findAll(globalSearch, pageable);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Class
     */
    public Class<Ingrediente> getEntityType() {
        return Ingrediente.class;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Class
     */
    public Class<Long> getIdType() {
        return Long.class;
    }
}
