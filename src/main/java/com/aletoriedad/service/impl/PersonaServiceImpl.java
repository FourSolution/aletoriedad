package com.aletoriedad.service.impl;
import com.aletoriedad.service.api.PersonaService;
import org.springframework.roo.addon.layers.service.annotations.RooServiceImpl;
import com.aletoriedad.model.Persona;
import com.aletoriedad.model.Usuario;
import com.aletoriedad.repository.PersonaRepository;
import com.aletoriedad.service.api.UsuarioService;
import io.springlets.data.domain.GlobalSearch;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * = PersonaServiceImpl
 *
 * TODO Auto-generated class documentation
 *
 */
@RooServiceImpl(service = PersonaService.class)
@Service
@Transactional(readOnly = true)
public class PersonaServiceImpl implements PersonaService {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private UsuarioService usuarioService;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private PersonaRepository personaRepository;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param personaRepository
     * @param usuarioService
     */
    @Autowired
    public PersonaServiceImpl(PersonaRepository personaRepository, @Lazy UsuarioService usuarioService) {
        setPersonaRepository(personaRepository);
        setUsuarioService(usuarioService);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return PersonaRepository
     */
    public PersonaRepository getPersonaRepository() {
        return personaRepository;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param personaRepository
     */
    public void setPersonaRepository(PersonaRepository personaRepository) {
        this.personaRepository = personaRepository;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return UsuarioService
     */
    public UsuarioService getUsuarioService() {
        return usuarioService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param usuarioService
     */
    public void setUsuarioService(UsuarioService usuarioService) {
        this.usuarioService = usuarioService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param persona
     * @param usuariosToAdd
     * @return Persona
     */
    @Transactional
    public Persona addToUsuarios(Persona persona, Iterable<Long> usuariosToAdd) {
        List<Usuario> usuarios = getUsuarioService().findAll(usuariosToAdd);
        persona.addToUsuarios(usuarios);
        return getPersonaRepository().save(persona);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param persona
     * @param usuariosToRemove
     * @return Persona
     */
    @Transactional
    public Persona removeFromUsuarios(Persona persona, Iterable<Long> usuariosToRemove) {
        List<Usuario> usuarios = getUsuarioService().findAll(usuariosToRemove);
        persona.removeFromUsuarios(usuarios);
        return getPersonaRepository().save(persona);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param persona
     * @param usuarios
     * @return Persona
     */
    @Transactional
    public Persona setUsuarios(Persona persona, Iterable<Long> usuarios) {
        List<Usuario> items = getUsuarioService().findAll(usuarios);
        Set<Usuario> currents = persona.getUsuarios();
        Set<Usuario> toRemove = new HashSet<Usuario>();
        for (Iterator<Usuario> iterator = currents.iterator(); iterator.hasNext(); ) {
            Usuario nextUsuario = iterator.next();
            if (items.contains(nextUsuario)) {
                items.remove(nextUsuario);
            } else {
                toRemove.add(nextUsuario);
            }
        }
        persona.removeFromUsuarios(toRemove);
        persona.addToUsuarios(items);
        // Force the version update of the parent side to know that the parent has changed
        // because it has new books assigned
        persona.setVersion(persona.getVersion() + 1);
        return getPersonaRepository().save(persona);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param persona
     */
    @Transactional
    public void delete(Persona persona) {
        // Clear bidirectional one-to-many parent relationship with Usuario
        for (Usuario item : persona.getUsuarios()) {
            item.setPersona(null);
        }
        getPersonaRepository().delete(persona);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param entities
     * @return List
     */
    @Transactional
    public List<Persona> save(Iterable<Persona> entities) {
        return getPersonaRepository().save(entities);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ids
     */
    @Transactional
    public void delete(Iterable<Long> ids) {
        List<Persona> toDelete = getPersonaRepository().findAll(ids);
        getPersonaRepository().deleteInBatch(toDelete);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param entity
     * @return Persona
     */
    @Transactional
    public Persona save(Persona entity) {
        return getPersonaRepository().save(entity);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @return Persona
     */
    public Persona findOne(Long id) {
        return getPersonaRepository().findOne(id);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @return Persona
     */
    public Persona findOneForUpdate(Long id) {
        return getPersonaRepository().findOneDetached(id);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ids
     * @return List
     */
    public List<Persona> findAll(Iterable<Long> ids) {
        return getPersonaRepository().findAll(ids);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return List
     */
    public List<Persona> findAll() {
        return getPersonaRepository().findAll();
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Long
     */
    public long count() {
        return getPersonaRepository().count();
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<Persona> findAll(GlobalSearch globalSearch, Pageable pageable) {
        return getPersonaRepository().findAll(globalSearch, pageable);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Class
     */
    public Class<Persona> getEntityType() {
        return Persona.class;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Class
     */
    public Class<Long> getIdType() {
        return Long.class;
    }
}
