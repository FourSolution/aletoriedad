package com.aletoriedad.service.impl;
import com.aletoriedad.service.api.PlatoService;
import org.springframework.roo.addon.layers.service.annotations.RooServiceImpl;
import com.aletoriedad.model.CuponCompra;
import com.aletoriedad.model.IngredientePlato;
import com.aletoriedad.model.Plato;
import com.aletoriedad.model.Restaurante;
import com.aletoriedad.repository.PlatoRepository;
import com.aletoriedad.service.api.CuponCompraService;
import com.aletoriedad.service.api.IngredientePlatoService;
import io.springlets.data.domain.GlobalSearch;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * = PlatoServiceImpl
 *
 * TODO Auto-generated class documentation
 *
 */
@RooServiceImpl(service = PlatoService.class)
@Service
@Transactional(readOnly = true)
public class PlatoServiceImpl implements PlatoService {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private CuponCompraService cuponCompraService;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private PlatoRepository platoRepository;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private IngredientePlatoService ingredientePlatoService;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param platoRepository
     * @param cuponCompraService
     * @param ingredientePlatoService
     */
    @Autowired
    public PlatoServiceImpl(PlatoRepository platoRepository, @Lazy CuponCompraService cuponCompraService, @Lazy IngredientePlatoService ingredientePlatoService) {
        setPlatoRepository(platoRepository);
        setCuponCompraService(cuponCompraService);
        setIngredientePlatoService(ingredientePlatoService);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return PlatoRepository
     */
    public PlatoRepository getPlatoRepository() {
        return platoRepository;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param platoRepository
     */
    public void setPlatoRepository(PlatoRepository platoRepository) {
        this.platoRepository = platoRepository;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return CuponCompraService
     */
    public CuponCompraService getCuponCompraService() {
        return cuponCompraService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param cuponCompraService
     */
    public void setCuponCompraService(CuponCompraService cuponCompraService) {
        this.cuponCompraService = cuponCompraService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return IngredientePlatoService
     */
    public IngredientePlatoService getIngredientePlatoService() {
        return ingredientePlatoService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingredientePlatoService
     */
    public void setIngredientePlatoService(IngredientePlatoService ingredientePlatoService) {
        this.ingredientePlatoService = ingredientePlatoService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @param ingredientesplatosToAdd
     * @return Plato
     */
    @Transactional
    public Plato addToIngredientesplatos(Plato plato, Iterable<Long> ingredientesplatosToAdd) {
        List<IngredientePlato> ingredientesplatos = getIngredientePlatoService().findAll(ingredientesplatosToAdd);
        plato.addToIngredientesplatos(ingredientesplatos);
        return getPlatoRepository().save(plato);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @param platocuponToAdd
     * @return Plato
     */
    @Transactional
    public Plato addToPlatocupon(Plato plato, Iterable<Long> platocuponToAdd) {
        List<CuponCompra> platocupon = getCuponCompraService().findAll(platocuponToAdd);
        plato.addToPlatocupon(platocupon);
        return getPlatoRepository().save(plato);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @param ingredientesplatosToRemove
     * @return Plato
     */
    @Transactional
    public Plato removeFromIngredientesplatos(Plato plato, Iterable<Long> ingredientesplatosToRemove) {
        List<IngredientePlato> ingredientesplatos = getIngredientePlatoService().findAll(ingredientesplatosToRemove);
        plato.removeFromIngredientesplatos(ingredientesplatos);
        return getPlatoRepository().save(plato);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @param platocuponToRemove
     * @return Plato
     */
    @Transactional
    public Plato removeFromPlatocupon(Plato plato, Iterable<Long> platocuponToRemove) {
        List<CuponCompra> platocupon = getCuponCompraService().findAll(platocuponToRemove);
        plato.removeFromPlatocupon(platocupon);
        return getPlatoRepository().save(plato);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @param ingredientesplatos
     * @return Plato
     */
    @Transactional
    public Plato setIngredientesplatos(Plato plato, Iterable<Long> ingredientesplatos) {
        List<IngredientePlato> items = getIngredientePlatoService().findAll(ingredientesplatos);
        Set<IngredientePlato> currents = plato.getIngredientesplatos();
        Set<IngredientePlato> toRemove = new HashSet<IngredientePlato>();
        for (Iterator<IngredientePlato> iterator = currents.iterator(); iterator.hasNext(); ) {
            IngredientePlato nextIngredientePlato = iterator.next();
            if (items.contains(nextIngredientePlato)) {
                items.remove(nextIngredientePlato);
            } else {
                toRemove.add(nextIngredientePlato);
            }
        }
        plato.removeFromIngredientesplatos(toRemove);
        plato.addToIngredientesplatos(items);
        // Force the version update of the parent side to know that the parent has changed
        // because it has new books assigned
        plato.setVersion(plato.getVersion() + 1);
        return getPlatoRepository().save(plato);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @param platocupon
     * @return Plato
     */
    @Transactional
    public Plato setPlatocupon(Plato plato, Iterable<Long> platocupon) {
        List<CuponCompra> items = getCuponCompraService().findAll(platocupon);
        Set<CuponCompra> currents = plato.getPlatocupon();
        Set<CuponCompra> toRemove = new HashSet<CuponCompra>();
        for (Iterator<CuponCompra> iterator = currents.iterator(); iterator.hasNext(); ) {
            CuponCompra nextCuponCompra = iterator.next();
            if (items.contains(nextCuponCompra)) {
                items.remove(nextCuponCompra);
            } else {
                toRemove.add(nextCuponCompra);
            }
        }
        plato.removeFromPlatocupon(toRemove);
        plato.addToPlatocupon(items);
        // Force the version update of the parent side to know that the parent has changed
        // because it has new books assigned
        plato.setVersion(plato.getVersion() + 1);
        return getPlatoRepository().save(plato);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     */
    @Transactional
    public void delete(Plato plato) {
        // Clear bidirectional many-to-one child relationship with Restaurante
        if (plato.getRestaurante() != null) {
            plato.getRestaurante().getPlatos().remove(plato);
        }
        // Clear bidirectional one-to-many parent relationship with IngredientePlato
        for (IngredientePlato item : plato.getIngredientesplatos()) {
            item.setPlato(null);
        }
        // Clear bidirectional one-to-many parent relationship with CuponCompra
        for (CuponCompra item : plato.getPlatocupon()) {
            item.setPlato(null);
        }
        getPlatoRepository().delete(plato);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param entities
     * @return List
     */
    @Transactional
    public List<Plato> save(Iterable<Plato> entities) {
        return getPlatoRepository().save(entities);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ids
     */
    @Transactional
    public void delete(Iterable<Long> ids) {
        List<Plato> toDelete = getPlatoRepository().findAll(ids);
        getPlatoRepository().deleteInBatch(toDelete);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param entity
     * @return Plato
     */
    @Transactional
    public Plato save(Plato entity) {
        return getPlatoRepository().save(entity);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @return Plato
     */
    public Plato findOne(Long id) {
        return getPlatoRepository().findOne(id);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @return Plato
     */
    public Plato findOneForUpdate(Long id) {
        return getPlatoRepository().findOneDetached(id);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ids
     * @return List
     */
    public List<Plato> findAll(Iterable<Long> ids) {
        return getPlatoRepository().findAll(ids);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return List
     */
    public List<Plato> findAll() {
        return getPlatoRepository().findAll();
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Long
     */
    public long count() {
        return getPlatoRepository().count();
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<Plato> findAll(GlobalSearch globalSearch, Pageable pageable) {
        return getPlatoRepository().findAll(globalSearch, pageable);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param restaurante
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<Plato> findByRestaurante(Restaurante restaurante, GlobalSearch globalSearch, Pageable pageable) {
        return getPlatoRepository().findByRestaurante(restaurante, globalSearch, pageable);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param restaurante
     * @return Long
     */
    public long countByRestaurante(Restaurante restaurante) {
        return getPlatoRepository().countByRestaurante(restaurante);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Class
     */
    public Class<Plato> getEntityType() {
        return Plato.class;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Class
     */
    public Class<Long> getIdType() {
        return Long.class;
    }
}
