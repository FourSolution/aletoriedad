package com.aletoriedad.service.impl;
import com.aletoriedad.service.api.RestauranteService;
import org.springframework.roo.addon.layers.service.annotations.RooServiceImpl;
import com.aletoriedad.model.Plato;
import com.aletoriedad.model.Restaurante;
import com.aletoriedad.repository.RestauranteRepository;
import com.aletoriedad.service.api.PlatoService;
import io.springlets.data.domain.GlobalSearch;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * = RestauranteServiceImpl
 *
 * TODO Auto-generated class documentation
 *
 */
@RooServiceImpl(service = RestauranteService.class)
@Service
@Transactional(readOnly = true)
public class RestauranteServiceImpl implements RestauranteService {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private RestauranteRepository restauranteRepository;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private PlatoService platoService;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param restauranteRepository
     * @param platoService
     */
    @Autowired
    public RestauranteServiceImpl(RestauranteRepository restauranteRepository, @Lazy PlatoService platoService) {
        setRestauranteRepository(restauranteRepository);
        setPlatoService(platoService);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return RestauranteRepository
     */
    public RestauranteRepository getRestauranteRepository() {
        return restauranteRepository;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param restauranteRepository
     */
    public void setRestauranteRepository(RestauranteRepository restauranteRepository) {
        this.restauranteRepository = restauranteRepository;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return PlatoService
     */
    public PlatoService getPlatoService() {
        return platoService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param platoService
     */
    public void setPlatoService(PlatoService platoService) {
        this.platoService = platoService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param restaurante
     * @param platosToAdd
     * @return Restaurante
     */
    @Transactional
    public Restaurante addToPlatos(Restaurante restaurante, Iterable<Long> platosToAdd) {
        List<Plato> platos = getPlatoService().findAll(platosToAdd);
        restaurante.addToPlatos(platos);
        return getRestauranteRepository().save(restaurante);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param restaurante
     * @param platosToRemove
     * @return Restaurante
     */
    @Transactional
    public Restaurante removeFromPlatos(Restaurante restaurante, Iterable<Long> platosToRemove) {
        List<Plato> platos = getPlatoService().findAll(platosToRemove);
        restaurante.removeFromPlatos(platos);
        return getRestauranteRepository().save(restaurante);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param restaurante
     * @param platos
     * @return Restaurante
     */
    @Transactional
    public Restaurante setPlatos(Restaurante restaurante, Iterable<Long> platos) {
        List<Plato> items = getPlatoService().findAll(platos);
        Set<Plato> currents = restaurante.getPlatos();
        Set<Plato> toRemove = new HashSet<Plato>();
        for (Iterator<Plato> iterator = currents.iterator(); iterator.hasNext(); ) {
            Plato nextPlato = iterator.next();
            if (items.contains(nextPlato)) {
                items.remove(nextPlato);
            } else {
                toRemove.add(nextPlato);
            }
        }
        restaurante.removeFromPlatos(toRemove);
        restaurante.addToPlatos(items);
        // Force the version update of the parent side to know that the parent has changed
        // because it has new books assigned
        restaurante.setVersion(restaurante.getVersion() + 1);
        return getRestauranteRepository().save(restaurante);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param restaurante
     */
    @Transactional
    public void delete(Restaurante restaurante) {
        // Clear bidirectional one-to-many parent relationship with Plato
        for (Plato item : restaurante.getPlatos()) {
            item.setRestaurante(null);
        }
        getRestauranteRepository().delete(restaurante);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param entities
     * @return List
     */
    @Transactional
    public List<Restaurante> save(Iterable<Restaurante> entities) {
        return getRestauranteRepository().save(entities);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ids
     */
    @Transactional
    public void delete(Iterable<Long> ids) {
        List<Restaurante> toDelete = getRestauranteRepository().findAll(ids);
        getRestauranteRepository().deleteInBatch(toDelete);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param entity
     * @return Restaurante
     */
    @Transactional
    public Restaurante save(Restaurante entity) {
        return getRestauranteRepository().save(entity);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @return Restaurante
     */
    public Restaurante findOne(Long id) {
        return getRestauranteRepository().findOne(id);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @return Restaurante
     */
    public Restaurante findOneForUpdate(Long id) {
        return getRestauranteRepository().findOneDetached(id);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ids
     * @return List
     */
    public List<Restaurante> findAll(Iterable<Long> ids) {
        return getRestauranteRepository().findAll(ids);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return List
     */
    public List<Restaurante> findAll() {
        return getRestauranteRepository().findAll();
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Long
     */
    public long count() {
        return getRestauranteRepository().count();
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<Restaurante> findAll(GlobalSearch globalSearch, Pageable pageable) {
        return getRestauranteRepository().findAll(globalSearch, pageable);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Class
     */
    public Class<Restaurante> getEntityType() {
        return Restaurante.class;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Class
     */
    public Class<Long> getIdType() {
        return Long.class;
    }
}
