package com.aletoriedad.service.impl;
import com.aletoriedad.service.api.UsuarioService;
import org.springframework.roo.addon.layers.service.annotations.RooServiceImpl;
import com.aletoriedad.model.CuponCompra;
import com.aletoriedad.model.Persona;
import com.aletoriedad.model.Usuario;
import com.aletoriedad.repository.UsuarioRepository;
import com.aletoriedad.service.api.CuponCompraService;
import io.springlets.data.domain.GlobalSearch;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * = UsuarioServiceImpl
 *
 * TODO Auto-generated class documentation
 *
 */
@RooServiceImpl(service = UsuarioService.class)
@Service
@Transactional(readOnly = true)
public class UsuarioServiceImpl implements UsuarioService {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private CuponCompraService cuponCompraService;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private UsuarioRepository usuarioRepository;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param usuarioRepository
     * @param cuponCompraService
     */
    @Autowired
    public UsuarioServiceImpl(UsuarioRepository usuarioRepository, @Lazy CuponCompraService cuponCompraService) {
        setUsuarioRepository(usuarioRepository);
        setCuponCompraService(cuponCompraService);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return UsuarioRepository
     */
    public UsuarioRepository getUsuarioRepository() {
        return usuarioRepository;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param usuarioRepository
     */
    public void setUsuarioRepository(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return CuponCompraService
     */
    public CuponCompraService getCuponCompraService() {
        return cuponCompraService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param cuponCompraService
     */
    public void setCuponCompraService(CuponCompraService cuponCompraService) {
        this.cuponCompraService = cuponCompraService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param usuario
     * @param usuariocuponToAdd
     * @return Usuario
     */
    @Transactional
    public Usuario addToUsuariocupon(Usuario usuario, Iterable<Long> usuariocuponToAdd) {
        List<CuponCompra> usuariocupon = getCuponCompraService().findAll(usuariocuponToAdd);
        usuario.addToUsuariocupon(usuariocupon);
        return getUsuarioRepository().save(usuario);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param usuario
     * @param usuariocuponToRemove
     * @return Usuario
     */
    @Transactional
    public Usuario removeFromUsuariocupon(Usuario usuario, Iterable<Long> usuariocuponToRemove) {
        List<CuponCompra> usuariocupon = getCuponCompraService().findAll(usuariocuponToRemove);
        usuario.removeFromUsuariocupon(usuariocupon);
        return getUsuarioRepository().save(usuario);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param usuario
     * @param usuariocupon
     * @return Usuario
     */
    @Transactional
    public Usuario setUsuariocupon(Usuario usuario, Iterable<Long> usuariocupon) {
        List<CuponCompra> items = getCuponCompraService().findAll(usuariocupon);
        Set<CuponCompra> currents = usuario.getUsuariocupon();
        Set<CuponCompra> toRemove = new HashSet<CuponCompra>();
        for (Iterator<CuponCompra> iterator = currents.iterator(); iterator.hasNext(); ) {
            CuponCompra nextCuponCompra = iterator.next();
            if (items.contains(nextCuponCompra)) {
                items.remove(nextCuponCompra);
            } else {
                toRemove.add(nextCuponCompra);
            }
        }
        usuario.removeFromUsuariocupon(toRemove);
        usuario.addToUsuariocupon(items);
        // Force the version update of the parent side to know that the parent has changed
        // because it has new books assigned
        usuario.setVersion(usuario.getVersion() + 1);
        return getUsuarioRepository().save(usuario);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param usuario
     */
    @Transactional
    public void delete(Usuario usuario) {
        // Clear bidirectional many-to-one child relationship with Persona
        if (usuario.getPersona() != null) {
            usuario.getPersona().getUsuarios().remove(usuario);
        }
        // Clear bidirectional one-to-many parent relationship with CuponCompra
        for (CuponCompra item : usuario.getUsuariocupon()) {
            item.setUsuario(null);
        }
        getUsuarioRepository().delete(usuario);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param entities
     * @return List
     */
    @Transactional
    public List<Usuario> save(Iterable<Usuario> entities) {
        return getUsuarioRepository().save(entities);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ids
     */
    @Transactional
    public void delete(Iterable<Long> ids) {
        List<Usuario> toDelete = getUsuarioRepository().findAll(ids);
        getUsuarioRepository().deleteInBatch(toDelete);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param entity
     * @return Usuario
     */
    @Transactional
    public Usuario save(Usuario entity) {
        return getUsuarioRepository().save(entity);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @return Usuario
     */
    public Usuario findOne(Long id) {
        return getUsuarioRepository().findOne(id);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @return Usuario
     */
    public Usuario findOneForUpdate(Long id) {
        return getUsuarioRepository().findOneDetached(id);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ids
     * @return List
     */
    public List<Usuario> findAll(Iterable<Long> ids) {
        return getUsuarioRepository().findAll(ids);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return List
     */
    public List<Usuario> findAll() {
        return getUsuarioRepository().findAll();
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Long
     */
    public long count() {
        return getUsuarioRepository().count();
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<Usuario> findAll(GlobalSearch globalSearch, Pageable pageable) {
        return getUsuarioRepository().findAll(globalSearch, pageable);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param persona
     * @param globalSearch
     * @param pageable
     * @return Page
     */
    public Page<Usuario> findByPersona(Persona persona, GlobalSearch globalSearch, Pageable pageable) {
        return getUsuarioRepository().findByPersona(persona, globalSearch, pageable);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param persona
     * @return Long
     */
    public long countByPersona(Persona persona) {
        return getUsuarioRepository().countByPersona(persona);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Class
     */
    public Class<Usuario> getEntityType() {
        return Usuario.class;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Class
     */
    public Class<Long> getIdType() {
        return Long.class;
    }
}
