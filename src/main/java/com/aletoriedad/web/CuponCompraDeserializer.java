package com.aletoriedad.web;
import com.aletoriedad.model.CuponCompra;
import com.aletoriedad.service.api.CuponCompraService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonObjectDeserializer;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.convert.ConversionService;
import org.springframework.roo.addon.web.mvc.controller.annotations.config.RooDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import io.springlets.web.NotFoundException;
import org.springframework.boot.jackson.JsonComponent;

/**
 * = CuponCompraDeserializer
 *
 * TODO Auto-generated class documentation
 *
 */
@RooDeserializer(entity = CuponCompra.class)
@JsonComponent
public class CuponCompraDeserializer extends JsonObjectDeserializer<CuponCompra> {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private CuponCompraService cuponCompraService;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private ConversionService conversionService;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param cuponCompraService
     * @param conversionService
     */
    @Autowired
    public CuponCompraDeserializer(@Lazy CuponCompraService cuponCompraService, ConversionService conversionService) {
        this.cuponCompraService = cuponCompraService;
        this.conversionService = conversionService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return CuponCompraService
     */
    public CuponCompraService getCuponCompraService() {
        return cuponCompraService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param cuponCompraService
     */
    public void setCuponCompraService(CuponCompraService cuponCompraService) {
        this.cuponCompraService = cuponCompraService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return ConversionService
     */
    public ConversionService getConversionService() {
        return conversionService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param conversionService
     */
    public void setConversionService(ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param jsonParser
     * @param context
     * @param codec
     * @param tree
     * @return CuponCompra
     * @throws IOException
     */
    public CuponCompra deserializeObject(JsonParser jsonParser, DeserializationContext context, ObjectCodec codec, JsonNode tree) {
        String idText = tree.asText();
        Long id = conversionService.convert(idText, Long.class);
        CuponCompra cuponCompra = cuponCompraService.findOne(id);
        if (cuponCompra == null) {
            throw new NotFoundException("CuponCompra not found");
        }
        return cuponCompra;
    }
}
