package com.aletoriedad.web;
import com.aletoriedad.model.CuponCompra;
import org.springframework.roo.addon.web.mvc.controller.annotations.config.RooJsonMixin;
import com.aletoriedad.model.Plato;
import com.aletoriedad.model.Usuario;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * = CuponCompraJsonMixin
 *
 * TODO Auto-generated class documentation
 *
 */
@RooJsonMixin(entity = CuponCompra.class)
public abstract class CuponCompraJsonMixin {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    @JsonDeserialize(using = UsuarioDeserializer.class)
    private Usuario usuario;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    @JsonDeserialize(using = PlatoDeserializer.class)
    private Plato plato;

    /**
     * TODO Auto-generated method documentation
     *
     * @return Plato
     */
    public Plato getPlato() {
        return plato;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     */
    public void setPlato(Plato plato) {
        this.plato = plato;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Usuario
     */
    public Usuario getUsuario() {
        return usuario;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param usuario
     */
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
