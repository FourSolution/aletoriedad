package com.aletoriedad.web;
import com.aletoriedad.model.CuponCompra;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.controller.annotations.responses.json.RooJSON;
import com.aletoriedad.service.api.CuponCompraService;
import io.springlets.data.domain.GlobalSearch;
import java.util.Collection;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.util.UriComponents;

/**
 * = CuponComprasCollectionJsonController
 *
 * TODO Auto-generated class documentation
 *
 */
@RooController(entity = CuponCompra.class, pathPrefix = "/api", type = ControllerType.COLLECTION)
@RooJSON
@RestController
@RequestMapping(value = "/api/cuponcompras", name = "CuponComprasCollectionJsonController", produces = MediaType.APPLICATION_JSON_VALUE)
public class CuponComprasCollectionJsonController {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private CuponCompraService cuponCompraService;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param cuponCompraService
     */
    @Autowired
    public CuponComprasCollectionJsonController(CuponCompraService cuponCompraService) {
        this.cuponCompraService = cuponCompraService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return CuponCompraService
     */
    public CuponCompraService getCuponCompraService() {
        return cuponCompraService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param cuponCompraService
     */
    public void setCuponCompraService(CuponCompraService cuponCompraService) {
        this.cuponCompraService = cuponCompraService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param globalSearch
     * @param pageable
     * @return ResponseEntity
     */
    @GetMapping(name = "list")
    public ResponseEntity<Page<CuponCompra>> list(GlobalSearch globalSearch, Pageable pageable) {
        Page<CuponCompra> cuponCompras = getCuponCompraService().findAll(globalSearch, pageable);
        return ResponseEntity.ok(cuponCompras);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return UriComponents
     */
    public static UriComponents listURI() {
        return MvcUriComponentsBuilder.fromMethodCall(MvcUriComponentsBuilder.on(CuponComprasCollectionJsonController.class).list(null, null)).build().encode();
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param cuponCompra
     * @param result
     * @return ResponseEntity
     */
    @PostMapping(name = "create")
    public ResponseEntity<?> create(@Valid @RequestBody CuponCompra cuponCompra, BindingResult result) {
        if (cuponCompra.getId() != null || cuponCompra.getVersion() != null) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
        if (result.hasErrors()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(result);
        }
        CuponCompra newCuponCompra = getCuponCompraService().save(cuponCompra);
        UriComponents showURI = CuponComprasItemJsonController.showURI(newCuponCompra);
        return ResponseEntity.created(showURI.toUri()).build();
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param cuponCompras
     * @param result
     * @return ResponseEntity
     */
    @PostMapping(value = "/batch", name = "createBatch")
    public ResponseEntity<?> createBatch(@Valid @RequestBody Collection<CuponCompra> cuponCompras, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(result);
        }
        getCuponCompraService().save(cuponCompras);
        return ResponseEntity.created(listURI().toUri()).build();
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param cuponCompras
     * @param result
     * @return ResponseEntity
     */
    @PutMapping(value = "/batch", name = "updateBatch")
    public ResponseEntity<?> updateBatch(@Valid @RequestBody Collection<CuponCompra> cuponCompras, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(result);
        }
        getCuponCompraService().save(cuponCompras);
        return ResponseEntity.ok().build();
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ids
     * @return ResponseEntity
     */
    @DeleteMapping(value = "/batch/{ids}", name = "deleteBatch")
    public ResponseEntity<?> deleteBatch(@PathVariable("ids") Collection<Long> ids) {
        getCuponCompraService().delete(ids);
        return ResponseEntity.ok().build();
    }
}
