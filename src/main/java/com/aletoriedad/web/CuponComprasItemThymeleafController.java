package com.aletoriedad.web;
import com.aletoriedad.model.CuponCompra;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.thymeleaf.annotations.RooThymeleaf;
import com.aletoriedad.service.api.CuponCompraService;
import io.springlets.web.NotFoundException;
import io.springlets.web.mvc.util.ControllerMethodLinkBuilderFactory;
import io.springlets.web.mvc.util.MethodLinkBuilderFactory;
import java.util.Locale;
import javax.validation.Valid;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponents;

/**
 * = CuponComprasItemThymeleafController
 *
 * TODO Auto-generated class documentation
 *
 */
@RooController(entity = CuponCompra.class, type = ControllerType.ITEM)
@RooThymeleaf
@Controller
@RequestMapping(value = "/cuponcompras/{cuponCompra}", name = "CuponComprasItemThymeleafController", produces = MediaType.TEXT_HTML_VALUE)
public class CuponComprasItemThymeleafController {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private CuponCompraService cuponCompraService;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private MethodLinkBuilderFactory<CuponComprasItemThymeleafController> itemLink;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private MessageSource messageSource;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param cuponCompraService
     * @param messageSource
     * @param linkBuilder
     */
    @Autowired
    public CuponComprasItemThymeleafController(CuponCompraService cuponCompraService, MessageSource messageSource, ControllerMethodLinkBuilderFactory linkBuilder) {
        setCuponCompraService(cuponCompraService);
        setMessageSource(messageSource);
        setItemLink(linkBuilder.of(CuponComprasItemThymeleafController.class));
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return CuponCompraService
     */
    public CuponCompraService getCuponCompraService() {
        return cuponCompraService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param cuponCompraService
     */
    public void setCuponCompraService(CuponCompraService cuponCompraService) {
        this.cuponCompraService = cuponCompraService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return MessageSource
     */
    public MessageSource getMessageSource() {
        return messageSource;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param messageSource
     */
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return MethodLinkBuilderFactory
     */
    public MethodLinkBuilderFactory<CuponComprasItemThymeleafController> getItemLink() {
        return itemLink;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param itemLink
     */
    public void setItemLink(MethodLinkBuilderFactory<CuponComprasItemThymeleafController> itemLink) {
        this.itemLink = itemLink;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @param locale
     * @param method
     * @return CuponCompra
     */
    @ModelAttribute
    public CuponCompra getCuponCompra(@PathVariable("cuponCompra") Long id, Locale locale, HttpMethod method) {
        CuponCompra cuponCompra = null;
        if (HttpMethod.PUT.equals(method)) {
            cuponCompra = cuponCompraService.findOneForUpdate(id);
        } else {
            cuponCompra = cuponCompraService.findOne(id);
        }
        if (cuponCompra == null) {
            String message = messageSource.getMessage("error_NotFound", new Object[] { "CuponCompra", id }, "The record couldn't be found", locale);
            throw new NotFoundException(message);
        }
        return cuponCompra;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param cuponCompra
     * @param model
     * @return ModelAndView
     */
    @GetMapping(name = "show")
    public ModelAndView show(@ModelAttribute CuponCompra cuponCompra, Model model) {
        model.addAttribute("cuponCompra", cuponCompra);
        return new ModelAndView("cuponcompras/show");
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param cuponCompra
     * @param model
     * @return ModelAndView
     */
    @GetMapping(value = "/inline", name = "showInline")
    public ModelAndView showInline(@ModelAttribute CuponCompra cuponCompra, Model model) {
        model.addAttribute("cuponCompra", cuponCompra);
        return new ModelAndView("cuponcompras/showInline :: inline-content");
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param dataBinder
     */
    @InitBinder("cuponCompra")
    public void initCuponCompraBinder(WebDataBinder dataBinder) {
        dataBinder.setDisallowedFields("id");
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param model
     */
    public void populateFormats(Model model) {
        model.addAttribute("application_locale", LocaleContextHolder.getLocale().getLanguage());
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param model
     */
    public void populateForm(Model model) {
        populateFormats(model);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param cuponCompra
     * @param model
     * @return ModelAndView
     */
    @GetMapping(value = "/edit-form", name = "editForm")
    public ModelAndView editForm(@ModelAttribute CuponCompra cuponCompra, Model model) {
        populateForm(model);
        model.addAttribute("cuponCompra", cuponCompra);
        return new ModelAndView("cuponcompras/edit");
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param cuponCompra
     * @param version
     * @param concurrencyControl
     * @param result
     * @param model
     * @return ModelAndView
     */
    @PutMapping(name = "update")
    public ModelAndView update(@Valid @ModelAttribute CuponCompra cuponCompra, @RequestParam("version") Integer version, @RequestParam(value = "concurrency", required = false, defaultValue = "") String concurrencyControl, BindingResult result, Model model) {
        // Check if provided form contain errors
        if (result.hasErrors()) {
            populateForm(model);
            return new ModelAndView("cuponcompras/edit");
        }
        // Concurrency control
        CuponCompra existingCuponCompra = getCuponCompraService().findOne(cuponCompra.getId());
        if (cuponCompra.getVersion() != existingCuponCompra.getVersion() && StringUtils.isEmpty(concurrencyControl)) {
            populateForm(model);
            model.addAttribute("cuponCompra", cuponCompra);
            model.addAttribute("concurrency", true);
            return new ModelAndView("cuponcompras/edit");
        } else if (cuponCompra.getVersion() != existingCuponCompra.getVersion() && "discard".equals(concurrencyControl)) {
            populateForm(model);
            model.addAttribute("cuponCompra", existingCuponCompra);
            model.addAttribute("concurrency", false);
            return new ModelAndView("cuponcompras/edit");
        } else if (cuponCompra.getVersion() != existingCuponCompra.getVersion() && "apply".equals(concurrencyControl)) {
            // Update the version field to be able to override the existing values
            cuponCompra.setVersion(existingCuponCompra.getVersion());
        }
        CuponCompra savedCuponCompra = getCuponCompraService().save(cuponCompra);
        UriComponents showURI = getItemLink().to(CuponComprasItemThymeleafLinkFactory.SHOW).with("cuponCompra", savedCuponCompra.getId()).toUri();
        return new ModelAndView("redirect:" + showURI.toUriString());
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param cuponCompra
     * @return ResponseEntity
     */
    @ResponseBody
    @DeleteMapping(name = "delete")
    public ResponseEntity<?> delete(@ModelAttribute CuponCompra cuponCompra) {
        getCuponCompraService().delete(cuponCompra);
        return ResponseEntity.ok().build();
    }
}
