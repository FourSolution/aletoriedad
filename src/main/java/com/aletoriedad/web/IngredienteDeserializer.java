package com.aletoriedad.web;
import com.aletoriedad.model.Ingrediente;
import com.aletoriedad.service.api.IngredienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonObjectDeserializer;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.convert.ConversionService;
import org.springframework.roo.addon.web.mvc.controller.annotations.config.RooDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import io.springlets.web.NotFoundException;
import org.springframework.boot.jackson.JsonComponent;

/**
 * = IngredienteDeserializer
 *
 * TODO Auto-generated class documentation
 *
 */
@RooDeserializer(entity = Ingrediente.class)
@JsonComponent
public class IngredienteDeserializer extends JsonObjectDeserializer<Ingrediente> {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private IngredienteService ingredienteService;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private ConversionService conversionService;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param ingredienteService
     * @param conversionService
     */
    @Autowired
    public IngredienteDeserializer(@Lazy IngredienteService ingredienteService, ConversionService conversionService) {
        this.ingredienteService = ingredienteService;
        this.conversionService = conversionService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return IngredienteService
     */
    public IngredienteService getIngredienteService() {
        return ingredienteService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingredienteService
     */
    public void setIngredienteService(IngredienteService ingredienteService) {
        this.ingredienteService = ingredienteService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return ConversionService
     */
    public ConversionService getConversionService() {
        return conversionService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param conversionService
     */
    public void setConversionService(ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param jsonParser
     * @param context
     * @param codec
     * @param tree
     * @return Ingrediente
     * @throws IOException
     */
    public Ingrediente deserializeObject(JsonParser jsonParser, DeserializationContext context, ObjectCodec codec, JsonNode tree) {
        String idText = tree.asText();
        Long id = conversionService.convert(idText, Long.class);
        Ingrediente ingrediente = ingredienteService.findOne(id);
        if (ingrediente == null) {
            throw new NotFoundException("Ingrediente not found");
        }
        return ingrediente;
    }
}
