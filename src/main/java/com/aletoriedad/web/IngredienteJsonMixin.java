package com.aletoriedad.web;
import com.aletoriedad.model.Ingrediente;
import org.springframework.roo.addon.web.mvc.controller.annotations.config.RooJsonMixin;
import com.aletoriedad.model.IngredientePlato;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Set;

/**
 * = IngredienteJsonMixin
 *
 * TODO Auto-generated class documentation
 *
 */
@RooJsonMixin(entity = Ingrediente.class)
public abstract class IngredienteJsonMixin {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    @JsonIgnore
    private Set<IngredientePlato> platosingredientes;

    /**
     * TODO Auto-generated method documentation
     *
     * @return Set
     */
    public Set<IngredientePlato> getPlatosingredientes() {
        return platosingredientes;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param platosingredientes
     */
    public void setPlatosingredientes(Set<IngredientePlato> platosingredientes) {
        this.platosingredientes = platosingredientes;
    }
}
