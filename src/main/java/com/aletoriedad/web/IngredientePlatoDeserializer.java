package com.aletoriedad.web;
import com.aletoriedad.model.IngredientePlato;
import com.aletoriedad.service.api.IngredientePlatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonObjectDeserializer;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.convert.ConversionService;
import org.springframework.roo.addon.web.mvc.controller.annotations.config.RooDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import io.springlets.web.NotFoundException;
import org.springframework.boot.jackson.JsonComponent;

/**
 * = IngredientePlatoDeserializer
 *
 * TODO Auto-generated class documentation
 *
 */
@RooDeserializer(entity = IngredientePlato.class)
@JsonComponent
public class IngredientePlatoDeserializer extends JsonObjectDeserializer<IngredientePlato> {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private IngredientePlatoService ingredientePlatoService;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private ConversionService conversionService;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param ingredientePlatoService
     * @param conversionService
     */
    @Autowired
    public IngredientePlatoDeserializer(@Lazy IngredientePlatoService ingredientePlatoService, ConversionService conversionService) {
        this.ingredientePlatoService = ingredientePlatoService;
        this.conversionService = conversionService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return IngredientePlatoService
     */
    public IngredientePlatoService getIngredientePlatoService() {
        return ingredientePlatoService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingredientePlatoService
     */
    public void setIngredientePlatoService(IngredientePlatoService ingredientePlatoService) {
        this.ingredientePlatoService = ingredientePlatoService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return ConversionService
     */
    public ConversionService getConversionService() {
        return conversionService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param conversionService
     */
    public void setConversionService(ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param jsonParser
     * @param context
     * @param codec
     * @param tree
     * @return IngredientePlato
     * @throws IOException
     */
    public IngredientePlato deserializeObject(JsonParser jsonParser, DeserializationContext context, ObjectCodec codec, JsonNode tree) {
        String idText = tree.asText();
        Long id = conversionService.convert(idText, Long.class);
        IngredientePlato ingredientePlato = ingredientePlatoService.findOne(id);
        if (ingredientePlato == null) {
            throw new NotFoundException("IngredientePlato not found");
        }
        return ingredientePlato;
    }
}
