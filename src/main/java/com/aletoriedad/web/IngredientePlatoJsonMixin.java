package com.aletoriedad.web;
import com.aletoriedad.model.IngredientePlato;
import org.springframework.roo.addon.web.mvc.controller.annotations.config.RooJsonMixin;
import com.aletoriedad.model.Ingrediente;
import com.aletoriedad.model.Plato;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

/**
 * = IngredientePlatoJsonMixin
 *
 * TODO Auto-generated class documentation
 *
 */
@RooJsonMixin(entity = IngredientePlato.class)
public abstract class IngredientePlatoJsonMixin {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    @JsonDeserialize(using = PlatoDeserializer.class)
    private Plato plato;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    @JsonDeserialize(using = IngredienteDeserializer.class)
    private Ingrediente ingrediente;

    /**
     * TODO Auto-generated method documentation
     *
     * @return Ingrediente
     */
    public Ingrediente getIngrediente() {
        return ingrediente;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingrediente
     */
    public void setIngrediente(Ingrediente ingrediente) {
        this.ingrediente = ingrediente;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Plato
     */
    public Plato getPlato() {
        return plato;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     */
    public void setPlato(Plato plato) {
        this.plato = plato;
    }
}
