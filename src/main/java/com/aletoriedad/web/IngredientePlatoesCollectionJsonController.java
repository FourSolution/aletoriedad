package com.aletoriedad.web;
import com.aletoriedad.model.IngredientePlato;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.controller.annotations.responses.json.RooJSON;
import com.aletoriedad.service.api.IngredientePlatoService;
import io.springlets.data.domain.GlobalSearch;
import java.util.Collection;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.util.UriComponents;

/**
 * = IngredientePlatoesCollectionJsonController
 *
 * TODO Auto-generated class documentation
 *
 */
@RooController(entity = IngredientePlato.class, pathPrefix = "/api", type = ControllerType.COLLECTION)
@RooJSON
@RestController
@RequestMapping(value = "/api/ingredienteplatoes", name = "IngredientePlatoesCollectionJsonController", produces = MediaType.APPLICATION_JSON_VALUE)
public class IngredientePlatoesCollectionJsonController {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private IngredientePlatoService ingredientePlatoService;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param ingredientePlatoService
     */
    @Autowired
    public IngredientePlatoesCollectionJsonController(IngredientePlatoService ingredientePlatoService) {
        this.ingredientePlatoService = ingredientePlatoService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return IngredientePlatoService
     */
    public IngredientePlatoService getIngredientePlatoService() {
        return ingredientePlatoService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingredientePlatoService
     */
    public void setIngredientePlatoService(IngredientePlatoService ingredientePlatoService) {
        this.ingredientePlatoService = ingredientePlatoService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param globalSearch
     * @param pageable
     * @return ResponseEntity
     */
    @GetMapping(name = "list")
    public ResponseEntity<Page<IngredientePlato>> list(GlobalSearch globalSearch, Pageable pageable) {
        Page<IngredientePlato> ingredientePlatoes = getIngredientePlatoService().findAll(globalSearch, pageable);
        return ResponseEntity.ok(ingredientePlatoes);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return UriComponents
     */
    public static UriComponents listURI() {
        return MvcUriComponentsBuilder.fromMethodCall(MvcUriComponentsBuilder.on(IngredientePlatoesCollectionJsonController.class).list(null, null)).build().encode();
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingredientePlato
     * @param result
     * @return ResponseEntity
     */
    @PostMapping(name = "create")
    public ResponseEntity<?> create(@Valid @RequestBody IngredientePlato ingredientePlato, BindingResult result) {
        if (ingredientePlato.getId() != null || ingredientePlato.getVersion() != null) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
        if (result.hasErrors()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(result);
        }
        IngredientePlato newIngredientePlato = getIngredientePlatoService().save(ingredientePlato);
        UriComponents showURI = IngredientePlatoesItemJsonController.showURI(newIngredientePlato);
        return ResponseEntity.created(showURI.toUri()).build();
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingredientePlatoes
     * @param result
     * @return ResponseEntity
     */
    @PostMapping(value = "/batch", name = "createBatch")
    public ResponseEntity<?> createBatch(@Valid @RequestBody Collection<IngredientePlato> ingredientePlatoes, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(result);
        }
        getIngredientePlatoService().save(ingredientePlatoes);
        return ResponseEntity.created(listURI().toUri()).build();
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingredientePlatoes
     * @param result
     * @return ResponseEntity
     */
    @PutMapping(value = "/batch", name = "updateBatch")
    public ResponseEntity<?> updateBatch(@Valid @RequestBody Collection<IngredientePlato> ingredientePlatoes, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(result);
        }
        getIngredientePlatoService().save(ingredientePlatoes);
        return ResponseEntity.ok().build();
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ids
     * @return ResponseEntity
     */
    @DeleteMapping(value = "/batch/{ids}", name = "deleteBatch")
    public ResponseEntity<?> deleteBatch(@PathVariable("ids") Collection<Long> ids) {
        getIngredientePlatoService().delete(ids);
        return ResponseEntity.ok().build();
    }
}
