package com.aletoriedad.web;
import com.aletoriedad.model.IngredientePlato;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.controller.annotations.responses.json.RooJSON;
import com.aletoriedad.service.api.IngredientePlatoService;
import io.springlets.web.NotFoundException;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.util.UriComponents;

/**
 * = IngredientePlatoesItemJsonController
 *
 * TODO Auto-generated class documentation
 *
 */
@RooController(entity = IngredientePlato.class, pathPrefix = "/api", type = ControllerType.ITEM)
@RooJSON
@RestController
@RequestMapping(value = "/api/ingredienteplatoes/{ingredientePlato}", name = "IngredientePlatoesItemJsonController", produces = MediaType.APPLICATION_JSON_VALUE)
public class IngredientePlatoesItemJsonController {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private IngredientePlatoService ingredientePlatoService;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param ingredientePlatoService
     */
    @Autowired
    public IngredientePlatoesItemJsonController(IngredientePlatoService ingredientePlatoService) {
        this.ingredientePlatoService = ingredientePlatoService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return IngredientePlatoService
     */
    public IngredientePlatoService getIngredientePlatoService() {
        return ingredientePlatoService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingredientePlatoService
     */
    public void setIngredientePlatoService(IngredientePlatoService ingredientePlatoService) {
        this.ingredientePlatoService = ingredientePlatoService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @return IngredientePlato
     */
    @ModelAttribute
    public IngredientePlato getIngredientePlato(@PathVariable("ingredientePlato") Long id) {
        IngredientePlato ingredientePlato = ingredientePlatoService.findOne(id);
        if (ingredientePlato == null) {
            throw new NotFoundException(String.format("IngredientePlato with identifier '%s' not found", id));
        }
        return ingredientePlato;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingredientePlato
     * @return ResponseEntity
     */
    @GetMapping(name = "show")
    public ResponseEntity<?> show(@ModelAttribute IngredientePlato ingredientePlato) {
        return ResponseEntity.ok(ingredientePlato);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingredientePlato
     * @return UriComponents
     */
    public static UriComponents showURI(IngredientePlato ingredientePlato) {
        return MvcUriComponentsBuilder.fromMethodCall(MvcUriComponentsBuilder.on(IngredientePlatoesItemJsonController.class).show(ingredientePlato)).buildAndExpand(ingredientePlato.getId()).encode();
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param storedIngredientePlato
     * @param ingredientePlato
     * @param result
     * @return ResponseEntity
     */
    @PutMapping(name = "update")
    public ResponseEntity<?> update(@ModelAttribute IngredientePlato storedIngredientePlato, @Valid @RequestBody IngredientePlato ingredientePlato, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(result);
        }
        ingredientePlato.setId(storedIngredientePlato.getId());
        getIngredientePlatoService().save(ingredientePlato);
        return ResponseEntity.ok().build();
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingredientePlato
     * @return ResponseEntity
     */
    @DeleteMapping(name = "delete")
    public ResponseEntity<?> delete(@ModelAttribute IngredientePlato ingredientePlato) {
        getIngredientePlatoService().delete(ingredientePlato);
        return ResponseEntity.ok().build();
    }
}
