package com.aletoriedad.web;
import com.aletoriedad.model.IngredientePlato;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.thymeleaf.annotations.RooThymeleaf;
import com.aletoriedad.service.api.IngredientePlatoService;
import io.springlets.web.NotFoundException;
import io.springlets.web.mvc.util.ControllerMethodLinkBuilderFactory;
import io.springlets.web.mvc.util.MethodLinkBuilderFactory;
import java.util.Locale;
import javax.validation.Valid;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponents;

/**
 * = IngredientePlatoesItemThymeleafController
 *
 * TODO Auto-generated class documentation
 *
 */
@RooController(entity = IngredientePlato.class, type = ControllerType.ITEM)
@RooThymeleaf
@Controller
@RequestMapping(value = "/ingredienteplatoes/{ingredientePlato}", name = "IngredientePlatoesItemThymeleafController", produces = MediaType.TEXT_HTML_VALUE)
public class IngredientePlatoesItemThymeleafController {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private MethodLinkBuilderFactory<IngredientePlatoesItemThymeleafController> itemLink;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private MessageSource messageSource;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private IngredientePlatoService ingredientePlatoService;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param ingredientePlatoService
     * @param messageSource
     * @param linkBuilder
     */
    @Autowired
    public IngredientePlatoesItemThymeleafController(IngredientePlatoService ingredientePlatoService, MessageSource messageSource, ControllerMethodLinkBuilderFactory linkBuilder) {
        setIngredientePlatoService(ingredientePlatoService);
        setMessageSource(messageSource);
        setItemLink(linkBuilder.of(IngredientePlatoesItemThymeleafController.class));
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return IngredientePlatoService
     */
    public IngredientePlatoService getIngredientePlatoService() {
        return ingredientePlatoService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingredientePlatoService
     */
    public void setIngredientePlatoService(IngredientePlatoService ingredientePlatoService) {
        this.ingredientePlatoService = ingredientePlatoService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return MessageSource
     */
    public MessageSource getMessageSource() {
        return messageSource;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param messageSource
     */
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return MethodLinkBuilderFactory
     */
    public MethodLinkBuilderFactory<IngredientePlatoesItemThymeleafController> getItemLink() {
        return itemLink;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param itemLink
     */
    public void setItemLink(MethodLinkBuilderFactory<IngredientePlatoesItemThymeleafController> itemLink) {
        this.itemLink = itemLink;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @param locale
     * @param method
     * @return IngredientePlato
     */
    @ModelAttribute
    public IngredientePlato getIngredientePlato(@PathVariable("ingredientePlato") Long id, Locale locale, HttpMethod method) {
        IngredientePlato ingredientePlato = null;
        if (HttpMethod.PUT.equals(method)) {
            ingredientePlato = ingredientePlatoService.findOneForUpdate(id);
        } else {
            ingredientePlato = ingredientePlatoService.findOne(id);
        }
        if (ingredientePlato == null) {
            String message = messageSource.getMessage("error_NotFound", new Object[] { "IngredientePlato", id }, "The record couldn't be found", locale);
            throw new NotFoundException(message);
        }
        return ingredientePlato;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingredientePlato
     * @param model
     * @return ModelAndView
     */
    @GetMapping(name = "show")
    public ModelAndView show(@ModelAttribute IngredientePlato ingredientePlato, Model model) {
        model.addAttribute("ingredientePlato", ingredientePlato);
        return new ModelAndView("ingredienteplatoes/show");
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingredientePlato
     * @param model
     * @return ModelAndView
     */
    @GetMapping(value = "/inline", name = "showInline")
    public ModelAndView showInline(@ModelAttribute IngredientePlato ingredientePlato, Model model) {
        model.addAttribute("ingredientePlato", ingredientePlato);
        return new ModelAndView("ingredienteplatoes/showInline :: inline-content");
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param dataBinder
     */
    @InitBinder("ingredientePlato")
    public void initIngredientePlatoBinder(WebDataBinder dataBinder) {
        dataBinder.setDisallowedFields("id");
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param model
     */
    public void populateFormats(Model model) {
        model.addAttribute("application_locale", LocaleContextHolder.getLocale().getLanguage());
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param model
     */
    public void populateForm(Model model) {
        populateFormats(model);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingredientePlato
     * @param model
     * @return ModelAndView
     */
    @GetMapping(value = "/edit-form", name = "editForm")
    public ModelAndView editForm(@ModelAttribute IngredientePlato ingredientePlato, Model model) {
        populateForm(model);
        model.addAttribute("ingredientePlato", ingredientePlato);
        return new ModelAndView("ingredienteplatoes/edit");
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingredientePlato
     * @param version
     * @param concurrencyControl
     * @param result
     * @param model
     * @return ModelAndView
     */
    @PutMapping(name = "update")
    public ModelAndView update(@Valid @ModelAttribute IngredientePlato ingredientePlato, @RequestParam("version") Integer version, @RequestParam(value = "concurrency", required = false, defaultValue = "") String concurrencyControl, BindingResult result, Model model) {
        // Check if provided form contain errors
        if (result.hasErrors()) {
            populateForm(model);
            return new ModelAndView("ingredienteplatoes/edit");
        }
        // Concurrency control
        IngredientePlato existingIngredientePlato = getIngredientePlatoService().findOne(ingredientePlato.getId());
        if (ingredientePlato.getVersion() != existingIngredientePlato.getVersion() && StringUtils.isEmpty(concurrencyControl)) {
            populateForm(model);
            model.addAttribute("ingredientePlato", ingredientePlato);
            model.addAttribute("concurrency", true);
            return new ModelAndView("ingredienteplatoes/edit");
        } else if (ingredientePlato.getVersion() != existingIngredientePlato.getVersion() && "discard".equals(concurrencyControl)) {
            populateForm(model);
            model.addAttribute("ingredientePlato", existingIngredientePlato);
            model.addAttribute("concurrency", false);
            return new ModelAndView("ingredienteplatoes/edit");
        } else if (ingredientePlato.getVersion() != existingIngredientePlato.getVersion() && "apply".equals(concurrencyControl)) {
            // Update the version field to be able to override the existing values
            ingredientePlato.setVersion(existingIngredientePlato.getVersion());
        }
        IngredientePlato savedIngredientePlato = getIngredientePlatoService().save(ingredientePlato);
        UriComponents showURI = getItemLink().to(IngredientePlatoesItemThymeleafLinkFactory.SHOW).with("ingredientePlato", savedIngredientePlato.getId()).toUri();
        return new ModelAndView("redirect:" + showURI.toUriString());
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingredientePlato
     * @return ResponseEntity
     */
    @ResponseBody
    @DeleteMapping(name = "delete")
    public ResponseEntity<?> delete(@ModelAttribute IngredientePlato ingredientePlato) {
        getIngredientePlatoService().delete(ingredientePlato);
        return ResponseEntity.ok().build();
    }
}
