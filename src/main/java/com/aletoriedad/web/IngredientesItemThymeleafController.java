package com.aletoriedad.web;
import com.aletoriedad.model.Ingrediente;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.thymeleaf.annotations.RooThymeleaf;
import com.aletoriedad.service.api.IngredienteService;
import io.springlets.web.NotFoundException;
import io.springlets.web.mvc.util.ControllerMethodLinkBuilderFactory;
import io.springlets.web.mvc.util.MethodLinkBuilderFactory;
import java.util.Locale;
import javax.validation.Valid;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponents;

/**
 * = IngredientesItemThymeleafController
 *
 * TODO Auto-generated class documentation
 *
 */
@RooController(entity = Ingrediente.class, type = ControllerType.ITEM)
@RooThymeleaf
@Controller
@RequestMapping(value = "/ingredientes/{ingrediente}", name = "IngredientesItemThymeleafController", produces = MediaType.TEXT_HTML_VALUE)
public class IngredientesItemThymeleafController {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private IngredienteService ingredienteService;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private MethodLinkBuilderFactory<IngredientesItemThymeleafController> itemLink;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private MessageSource messageSource;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param ingredienteService
     * @param messageSource
     * @param linkBuilder
     */
    @Autowired
    public IngredientesItemThymeleafController(IngredienteService ingredienteService, MessageSource messageSource, ControllerMethodLinkBuilderFactory linkBuilder) {
        setIngredienteService(ingredienteService);
        setMessageSource(messageSource);
        setItemLink(linkBuilder.of(IngredientesItemThymeleafController.class));
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return IngredienteService
     */
    public IngredienteService getIngredienteService() {
        return ingredienteService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingredienteService
     */
    public void setIngredienteService(IngredienteService ingredienteService) {
        this.ingredienteService = ingredienteService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return MessageSource
     */
    public MessageSource getMessageSource() {
        return messageSource;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param messageSource
     */
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return MethodLinkBuilderFactory
     */
    public MethodLinkBuilderFactory<IngredientesItemThymeleafController> getItemLink() {
        return itemLink;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param itemLink
     */
    public void setItemLink(MethodLinkBuilderFactory<IngredientesItemThymeleafController> itemLink) {
        this.itemLink = itemLink;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @param locale
     * @param method
     * @return Ingrediente
     */
    @ModelAttribute
    public Ingrediente getIngrediente(@PathVariable("ingrediente") Long id, Locale locale, HttpMethod method) {
        Ingrediente ingrediente = null;
        if (HttpMethod.PUT.equals(method)) {
            ingrediente = ingredienteService.findOneForUpdate(id);
        } else {
            ingrediente = ingredienteService.findOne(id);
        }
        if (ingrediente == null) {
            String message = messageSource.getMessage("error_NotFound", new Object[] { "Ingrediente", id }, "The record couldn't be found", locale);
            throw new NotFoundException(message);
        }
        return ingrediente;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingrediente
     * @param model
     * @return ModelAndView
     */
    @GetMapping(name = "show")
    public ModelAndView show(@ModelAttribute Ingrediente ingrediente, Model model) {
        model.addAttribute("ingrediente", ingrediente);
        return new ModelAndView("ingredientes/show");
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingrediente
     * @param model
     * @return ModelAndView
     */
    @GetMapping(value = "/inline", name = "showInline")
    public ModelAndView showInline(@ModelAttribute Ingrediente ingrediente, Model model) {
        model.addAttribute("ingrediente", ingrediente);
        return new ModelAndView("ingredientes/showInline :: inline-content");
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param dataBinder
     */
    @InitBinder("ingrediente")
    public void initIngredienteBinder(WebDataBinder dataBinder) {
        dataBinder.setDisallowedFields("id");
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param model
     */
    public void populateFormats(Model model) {
        model.addAttribute("application_locale", LocaleContextHolder.getLocale().getLanguage());
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param model
     */
    public void populateForm(Model model) {
        populateFormats(model);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingrediente
     * @param model
     * @return ModelAndView
     */
    @GetMapping(value = "/edit-form", name = "editForm")
    public ModelAndView editForm(@ModelAttribute Ingrediente ingrediente, Model model) {
        populateForm(model);
        model.addAttribute("ingrediente", ingrediente);
        return new ModelAndView("ingredientes/edit");
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingrediente
     * @param version
     * @param concurrencyControl
     * @param result
     * @param model
     * @return ModelAndView
     */
    @PutMapping(name = "update")
    public ModelAndView update(@Valid @ModelAttribute Ingrediente ingrediente, @RequestParam("version") Integer version, @RequestParam(value = "concurrency", required = false, defaultValue = "") String concurrencyControl, BindingResult result, Model model) {
        // Check if provided form contain errors
        if (result.hasErrors()) {
            populateForm(model);
            return new ModelAndView("ingredientes/edit");
        }
        // Concurrency control
        Ingrediente existingIngrediente = getIngredienteService().findOne(ingrediente.getId());
        if (ingrediente.getVersion() != existingIngrediente.getVersion() && StringUtils.isEmpty(concurrencyControl)) {
            populateForm(model);
            model.addAttribute("ingrediente", ingrediente);
            model.addAttribute("concurrency", true);
            return new ModelAndView("ingredientes/edit");
        } else if (ingrediente.getVersion() != existingIngrediente.getVersion() && "discard".equals(concurrencyControl)) {
            populateForm(model);
            model.addAttribute("ingrediente", existingIngrediente);
            model.addAttribute("concurrency", false);
            return new ModelAndView("ingredientes/edit");
        } else if (ingrediente.getVersion() != existingIngrediente.getVersion() && "apply".equals(concurrencyControl)) {
            // Update the version field to be able to override the existing values
            ingrediente.setVersion(existingIngrediente.getVersion());
        }
        Ingrediente savedIngrediente = getIngredienteService().save(ingrediente);
        UriComponents showURI = getItemLink().to(IngredientesItemThymeleafLinkFactory.SHOW).with("ingrediente", savedIngrediente.getId()).toUri();
        return new ModelAndView("redirect:" + showURI.toUriString());
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingrediente
     * @return ResponseEntity
     */
    @ResponseBody
    @DeleteMapping(name = "delete")
    public ResponseEntity<?> delete(@ModelAttribute Ingrediente ingrediente) {
        getIngredienteService().delete(ingrediente);
        return ResponseEntity.ok().build();
    }
}
