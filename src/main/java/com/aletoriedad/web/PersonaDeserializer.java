package com.aletoriedad.web;
import com.aletoriedad.model.Persona;
import com.aletoriedad.service.api.PersonaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonObjectDeserializer;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.convert.ConversionService;
import org.springframework.roo.addon.web.mvc.controller.annotations.config.RooDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import io.springlets.web.NotFoundException;
import org.springframework.boot.jackson.JsonComponent;

/**
 * = PersonaDeserializer
 *
 * TODO Auto-generated class documentation
 *
 */
@RooDeserializer(entity = Persona.class)
@JsonComponent
public class PersonaDeserializer extends JsonObjectDeserializer<Persona> {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private PersonaService personaService;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private ConversionService conversionService;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param personaService
     * @param conversionService
     */
    @Autowired
    public PersonaDeserializer(@Lazy PersonaService personaService, ConversionService conversionService) {
        this.personaService = personaService;
        this.conversionService = conversionService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return PersonaService
     */
    public PersonaService getPersonaService() {
        return personaService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param personaService
     */
    public void setPersonaService(PersonaService personaService) {
        this.personaService = personaService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return ConversionService
     */
    public ConversionService getConversionService() {
        return conversionService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param conversionService
     */
    public void setConversionService(ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param jsonParser
     * @param context
     * @param codec
     * @param tree
     * @return Persona
     * @throws IOException
     */
    public Persona deserializeObject(JsonParser jsonParser, DeserializationContext context, ObjectCodec codec, JsonNode tree) {
        String idText = tree.asText();
        Long id = conversionService.convert(idText, Long.class);
        Persona persona = personaService.findOne(id);
        if (persona == null) {
            throw new NotFoundException("Persona not found");
        }
        return persona;
    }
}
