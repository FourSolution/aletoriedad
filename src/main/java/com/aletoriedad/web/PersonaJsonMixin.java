package com.aletoriedad.web;
import com.aletoriedad.model.Persona;
import org.springframework.roo.addon.web.mvc.controller.annotations.config.RooJsonMixin;
import com.aletoriedad.model.Usuario;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Set;

/**
 * = PersonaJsonMixin
 *
 * TODO Auto-generated class documentation
 *
 */
@RooJsonMixin(entity = Persona.class)
public abstract class PersonaJsonMixin {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    @JsonIgnore
    private Set<Usuario> usuarios;

    /**
     * TODO Auto-generated method documentation
     *
     * @return Set
     */
    public Set<Usuario> getUsuarios() {
        return usuarios;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param usuarios
     */
    public void setUsuarios(Set<Usuario> usuarios) {
        this.usuarios = usuarios;
    }
}
