package com.aletoriedad.web;
import com.aletoriedad.model.Persona;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.controller.annotations.responses.json.RooJSON;
import com.aletoriedad.service.api.PersonaService;
import io.springlets.web.NotFoundException;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.util.UriComponents;

/**
 * = PersonaeItemJsonController
 *
 * TODO Auto-generated class documentation
 *
 */
@RooController(entity = Persona.class, pathPrefix = "/api", type = ControllerType.ITEM)
@RooJSON
@RestController
@RequestMapping(value = "/api/personae/{persona}", name = "PersonaeItemJsonController", produces = MediaType.APPLICATION_JSON_VALUE)
public class PersonaeItemJsonController {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private PersonaService personaService;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param personaService
     */
    @Autowired
    public PersonaeItemJsonController(PersonaService personaService) {
        this.personaService = personaService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return PersonaService
     */
    public PersonaService getPersonaService() {
        return personaService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param personaService
     */
    public void setPersonaService(PersonaService personaService) {
        this.personaService = personaService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @return Persona
     */
    @ModelAttribute
    public Persona getPersona(@PathVariable("persona") Long id) {
        Persona persona = personaService.findOne(id);
        if (persona == null) {
            throw new NotFoundException(String.format("Persona with identifier '%s' not found", id));
        }
        return persona;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param persona
     * @return ResponseEntity
     */
    @GetMapping(name = "show")
    public ResponseEntity<?> show(@ModelAttribute Persona persona) {
        return ResponseEntity.ok(persona);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param persona
     * @return UriComponents
     */
    public static UriComponents showURI(Persona persona) {
        return MvcUriComponentsBuilder.fromMethodCall(MvcUriComponentsBuilder.on(PersonaeItemJsonController.class).show(persona)).buildAndExpand(persona.getId()).encode();
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param storedPersona
     * @param persona
     * @param result
     * @return ResponseEntity
     */
    @PutMapping(name = "update")
    public ResponseEntity<?> update(@ModelAttribute Persona storedPersona, @Valid @RequestBody Persona persona, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(result);
        }
        persona.setId(storedPersona.getId());
        getPersonaService().save(persona);
        return ResponseEntity.ok().build();
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param persona
     * @return ResponseEntity
     */
    @DeleteMapping(name = "delete")
    public ResponseEntity<?> delete(@ModelAttribute Persona persona) {
        getPersonaService().delete(persona);
        return ResponseEntity.ok().build();
    }
}
