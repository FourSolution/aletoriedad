package com.aletoriedad.web;
import com.aletoriedad.model.Persona;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.thymeleaf.annotations.RooThymeleaf;
import com.aletoriedad.service.api.PersonaService;
import io.springlets.web.NotFoundException;
import io.springlets.web.mvc.util.ControllerMethodLinkBuilderFactory;
import io.springlets.web.mvc.util.MethodLinkBuilderFactory;
import java.util.Locale;
import javax.validation.Valid;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponents;

/**
 * = PersonaeItemThymeleafController
 *
 * TODO Auto-generated class documentation
 *
 */
@RooController(entity = Persona.class, type = ControllerType.ITEM)
@RooThymeleaf
@Controller
@RequestMapping(value = "/personae/{persona}", name = "PersonaeItemThymeleafController", produces = MediaType.TEXT_HTML_VALUE)
public class PersonaeItemThymeleafController {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private PersonaService personaService;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private MethodLinkBuilderFactory<PersonaeItemThymeleafController> itemLink;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private MessageSource messageSource;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param personaService
     * @param messageSource
     * @param linkBuilder
     */
    @Autowired
    public PersonaeItemThymeleafController(PersonaService personaService, MessageSource messageSource, ControllerMethodLinkBuilderFactory linkBuilder) {
        setPersonaService(personaService);
        setMessageSource(messageSource);
        setItemLink(linkBuilder.of(PersonaeItemThymeleafController.class));
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return PersonaService
     */
    public PersonaService getPersonaService() {
        return personaService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param personaService
     */
    public void setPersonaService(PersonaService personaService) {
        this.personaService = personaService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return MessageSource
     */
    public MessageSource getMessageSource() {
        return messageSource;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param messageSource
     */
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return MethodLinkBuilderFactory
     */
    public MethodLinkBuilderFactory<PersonaeItemThymeleafController> getItemLink() {
        return itemLink;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param itemLink
     */
    public void setItemLink(MethodLinkBuilderFactory<PersonaeItemThymeleafController> itemLink) {
        this.itemLink = itemLink;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @param locale
     * @param method
     * @return Persona
     */
    @ModelAttribute
    public Persona getPersona(@PathVariable("persona") Long id, Locale locale, HttpMethod method) {
        Persona persona = null;
        if (HttpMethod.PUT.equals(method)) {
            persona = personaService.findOneForUpdate(id);
        } else {
            persona = personaService.findOne(id);
        }
        if (persona == null) {
            String message = messageSource.getMessage("error_NotFound", new Object[] { "Persona", id }, "The record couldn't be found", locale);
            throw new NotFoundException(message);
        }
        return persona;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param persona
     * @param model
     * @return ModelAndView
     */
    @GetMapping(name = "show")
    public ModelAndView show(@ModelAttribute Persona persona, Model model) {
        model.addAttribute("persona", persona);
        return new ModelAndView("personae/show");
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param persona
     * @param model
     * @return ModelAndView
     */
    @GetMapping(value = "/inline", name = "showInline")
    public ModelAndView showInline(@ModelAttribute Persona persona, Model model) {
        model.addAttribute("persona", persona);
        return new ModelAndView("personae/showInline :: inline-content");
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param dataBinder
     */
    @InitBinder("persona")
    public void initPersonaBinder(WebDataBinder dataBinder) {
        dataBinder.setDisallowedFields("id");
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param model
     */
    public void populateFormats(Model model) {
        model.addAttribute("application_locale", LocaleContextHolder.getLocale().getLanguage());
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param model
     */
    public void populateForm(Model model) {
        populateFormats(model);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param persona
     * @param model
     * @return ModelAndView
     */
    @GetMapping(value = "/edit-form", name = "editForm")
    public ModelAndView editForm(@ModelAttribute Persona persona, Model model) {
        populateForm(model);
        model.addAttribute("persona", persona);
        return new ModelAndView("personae/edit");
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param persona
     * @param version
     * @param concurrencyControl
     * @param result
     * @param model
     * @return ModelAndView
     */
    @PutMapping(name = "update")
    public ModelAndView update(@Valid @ModelAttribute Persona persona, @RequestParam("version") Integer version, @RequestParam(value = "concurrency", required = false, defaultValue = "") String concurrencyControl, BindingResult result, Model model) {
        // Check if provided form contain errors
        if (result.hasErrors()) {
            populateForm(model);
            return new ModelAndView("personae/edit");
        }
        // Concurrency control
        Persona existingPersona = getPersonaService().findOne(persona.getId());
        if (persona.getVersion() != existingPersona.getVersion() && StringUtils.isEmpty(concurrencyControl)) {
            populateForm(model);
            model.addAttribute("persona", persona);
            model.addAttribute("concurrency", true);
            return new ModelAndView("personae/edit");
        } else if (persona.getVersion() != existingPersona.getVersion() && "discard".equals(concurrencyControl)) {
            populateForm(model);
            model.addAttribute("persona", existingPersona);
            model.addAttribute("concurrency", false);
            return new ModelAndView("personae/edit");
        } else if (persona.getVersion() != existingPersona.getVersion() && "apply".equals(concurrencyControl)) {
            // Update the version field to be able to override the existing values
            persona.setVersion(existingPersona.getVersion());
        }
        Persona savedPersona = getPersonaService().save(persona);
        UriComponents showURI = getItemLink().to(PersonaeItemThymeleafLinkFactory.SHOW).with("persona", savedPersona.getId()).toUri();
        return new ModelAndView("redirect:" + showURI.toUriString());
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param persona
     * @return ResponseEntity
     */
    @ResponseBody
    @DeleteMapping(name = "delete")
    public ResponseEntity<?> delete(@ModelAttribute Persona persona) {
        getPersonaService().delete(persona);
        return ResponseEntity.ok().build();
    }
}
