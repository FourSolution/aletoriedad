package com.aletoriedad.web;
import com.aletoriedad.model.Plato;
import com.aletoriedad.service.api.PlatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonObjectDeserializer;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.convert.ConversionService;
import org.springframework.roo.addon.web.mvc.controller.annotations.config.RooDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import io.springlets.web.NotFoundException;
import org.springframework.boot.jackson.JsonComponent;

/**
 * = PlatoDeserializer
 *
 * TODO Auto-generated class documentation
 *
 */
@RooDeserializer(entity = Plato.class)
@JsonComponent
public class PlatoDeserializer extends JsonObjectDeserializer<Plato> {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private PlatoService platoService;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private ConversionService conversionService;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param platoService
     * @param conversionService
     */
    @Autowired
    public PlatoDeserializer(@Lazy PlatoService platoService, ConversionService conversionService) {
        this.platoService = platoService;
        this.conversionService = conversionService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return PlatoService
     */
    public PlatoService getPlatoService() {
        return platoService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param platoService
     */
    public void setPlatoService(PlatoService platoService) {
        this.platoService = platoService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return ConversionService
     */
    public ConversionService getConversionService() {
        return conversionService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param conversionService
     */
    public void setConversionService(ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param jsonParser
     * @param context
     * @param codec
     * @param tree
     * @return Plato
     * @throws IOException
     */
    public Plato deserializeObject(JsonParser jsonParser, DeserializationContext context, ObjectCodec codec, JsonNode tree) {
        String idText = tree.asText();
        Long id = conversionService.convert(idText, Long.class);
        Plato plato = platoService.findOne(id);
        if (plato == null) {
            throw new NotFoundException("Plato not found");
        }
        return plato;
    }
}
