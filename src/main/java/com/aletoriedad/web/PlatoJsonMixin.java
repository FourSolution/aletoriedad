package com.aletoriedad.web;
import com.aletoriedad.model.Plato;
import org.springframework.roo.addon.web.mvc.controller.annotations.config.RooJsonMixin;
import com.aletoriedad.model.CuponCompra;
import com.aletoriedad.model.IngredientePlato;
import com.aletoriedad.model.Restaurante;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.util.Set;

/**
 * = PlatoJsonMixin
 *
 * TODO Auto-generated class documentation
 *
 */
@RooJsonMixin(entity = Plato.class)
public abstract class PlatoJsonMixin {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    @JsonIgnore
    private Set<IngredientePlato> ingredientesplatos;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    @JsonIgnore
    private Set<CuponCompra> platocupon;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    @JsonDeserialize(using = RestauranteDeserializer.class)
    private Restaurante restaurante;

    /**
     * TODO Auto-generated method documentation
     *
     * @return Set
     */
    public Set<IngredientePlato> getIngredientesplatos() {
        return ingredientesplatos;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param ingredientesplatos
     */
    public void setIngredientesplatos(Set<IngredientePlato> ingredientesplatos) {
        this.ingredientesplatos = ingredientesplatos;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Set
     */
    public Set<CuponCompra> getPlatocupon() {
        return platocupon;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param platocupon
     */
    public void setPlatocupon(Set<CuponCompra> platocupon) {
        this.platocupon = platocupon;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Restaurante
     */
    public Restaurante getRestaurante() {
        return restaurante;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param restaurante
     */
    public void setRestaurante(Restaurante restaurante) {
        this.restaurante = restaurante;
    }
}
