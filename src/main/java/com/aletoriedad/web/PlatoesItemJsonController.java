package com.aletoriedad.web;
import com.aletoriedad.model.Plato;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.controller.annotations.responses.json.RooJSON;
import com.aletoriedad.service.api.PlatoService;
import io.springlets.web.NotFoundException;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.util.UriComponents;

/**
 * = PlatoesItemJsonController
 *
 * TODO Auto-generated class documentation
 *
 */
@RooController(entity = Plato.class, pathPrefix = "/api", type = ControllerType.ITEM)
@RooJSON
@RestController
@RequestMapping(value = "/api/platoes/{plato}", name = "PlatoesItemJsonController", produces = MediaType.APPLICATION_JSON_VALUE)
public class PlatoesItemJsonController {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private PlatoService platoService;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param platoService
     */
    @Autowired
    public PlatoesItemJsonController(PlatoService platoService) {
        this.platoService = platoService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return PlatoService
     */
    public PlatoService getPlatoService() {
        return platoService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param platoService
     */
    public void setPlatoService(PlatoService platoService) {
        this.platoService = platoService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @return Plato
     */
    @ModelAttribute
    public Plato getPlato(@PathVariable("plato") Long id) {
        Plato plato = platoService.findOne(id);
        if (plato == null) {
            throw new NotFoundException(String.format("Plato with identifier '%s' not found", id));
        }
        return plato;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @return ResponseEntity
     */
    @GetMapping(name = "show")
    public ResponseEntity<?> show(@ModelAttribute Plato plato) {
        return ResponseEntity.ok(plato);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @return UriComponents
     */
    public static UriComponents showURI(Plato plato) {
        return MvcUriComponentsBuilder.fromMethodCall(MvcUriComponentsBuilder.on(PlatoesItemJsonController.class).show(plato)).buildAndExpand(plato.getId()).encode();
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param storedPlato
     * @param plato
     * @param result
     * @return ResponseEntity
     */
    @PutMapping(name = "update")
    public ResponseEntity<?> update(@ModelAttribute Plato storedPlato, @Valid @RequestBody Plato plato, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(result);
        }
        plato.setId(storedPlato.getId());
        getPlatoService().save(plato);
        return ResponseEntity.ok().build();
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @return ResponseEntity
     */
    @DeleteMapping(name = "delete")
    public ResponseEntity<?> delete(@ModelAttribute Plato plato) {
        getPlatoService().delete(plato);
        return ResponseEntity.ok().build();
    }
}
