package com.aletoriedad.web;
import com.aletoriedad.model.Plato;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.thymeleaf.annotations.RooThymeleaf;
import com.aletoriedad.service.api.PlatoService;
import io.springlets.web.NotFoundException;
import io.springlets.web.mvc.util.ControllerMethodLinkBuilderFactory;
import io.springlets.web.mvc.util.MethodLinkBuilderFactory;
import java.util.Locale;
import javax.validation.Valid;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponents;

/**
 * = PlatoesItemThymeleafController
 *
 * TODO Auto-generated class documentation
 *
 */
@RooController(entity = Plato.class, type = ControllerType.ITEM)
@RooThymeleaf
@Controller
@RequestMapping(value = "/platoes/{plato}", name = "PlatoesItemThymeleafController", produces = MediaType.TEXT_HTML_VALUE)
public class PlatoesItemThymeleafController {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private MethodLinkBuilderFactory<PlatoesItemThymeleafController> itemLink;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private MessageSource messageSource;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private PlatoService platoService;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param platoService
     * @param messageSource
     * @param linkBuilder
     */
    @Autowired
    public PlatoesItemThymeleafController(PlatoService platoService, MessageSource messageSource, ControllerMethodLinkBuilderFactory linkBuilder) {
        setPlatoService(platoService);
        setMessageSource(messageSource);
        setItemLink(linkBuilder.of(PlatoesItemThymeleafController.class));
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return PlatoService
     */
    public PlatoService getPlatoService() {
        return platoService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param platoService
     */
    public void setPlatoService(PlatoService platoService) {
        this.platoService = platoService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return MessageSource
     */
    public MessageSource getMessageSource() {
        return messageSource;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param messageSource
     */
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return MethodLinkBuilderFactory
     */
    public MethodLinkBuilderFactory<PlatoesItemThymeleafController> getItemLink() {
        return itemLink;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param itemLink
     */
    public void setItemLink(MethodLinkBuilderFactory<PlatoesItemThymeleafController> itemLink) {
        this.itemLink = itemLink;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @param locale
     * @param method
     * @return Plato
     */
    @ModelAttribute
    public Plato getPlato(@PathVariable("plato") Long id, Locale locale, HttpMethod method) {
        Plato plato = null;
        if (HttpMethod.PUT.equals(method)) {
            plato = platoService.findOneForUpdate(id);
        } else {
            plato = platoService.findOne(id);
        }
        if (plato == null) {
            String message = messageSource.getMessage("error_NotFound", new Object[] { "Plato", id }, "The record couldn't be found", locale);
            throw new NotFoundException(message);
        }
        return plato;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @param model
     * @return ModelAndView
     */
    @GetMapping(name = "show")
    public ModelAndView show(@ModelAttribute Plato plato, Model model) {
        model.addAttribute("plato", plato);
        return new ModelAndView("platoes/show");
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @param model
     * @return ModelAndView
     */
    @GetMapping(value = "/inline", name = "showInline")
    public ModelAndView showInline(@ModelAttribute Plato plato, Model model) {
        model.addAttribute("plato", plato);
        return new ModelAndView("platoes/showInline :: inline-content");
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param dataBinder
     */
    @InitBinder("plato")
    public void initPlatoBinder(WebDataBinder dataBinder) {
        dataBinder.setDisallowedFields("id");
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param model
     */
    public void populateFormats(Model model) {
        model.addAttribute("application_locale", LocaleContextHolder.getLocale().getLanguage());
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param model
     */
    public void populateForm(Model model) {
        populateFormats(model);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @param model
     * @return ModelAndView
     */
    @GetMapping(value = "/edit-form", name = "editForm")
    public ModelAndView editForm(@ModelAttribute Plato plato, Model model) {
        populateForm(model);
        model.addAttribute("plato", plato);
        return new ModelAndView("platoes/edit");
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @param version
     * @param concurrencyControl
     * @param result
     * @param model
     * @return ModelAndView
     */
    @PutMapping(name = "update")
    public ModelAndView update(@Valid @ModelAttribute Plato plato, @RequestParam("version") Integer version, @RequestParam(value = "concurrency", required = false, defaultValue = "") String concurrencyControl, BindingResult result, Model model) {
        // Check if provided form contain errors
        if (result.hasErrors()) {
            populateForm(model);
            return new ModelAndView("platoes/edit");
        }
        // Concurrency control
        Plato existingPlato = getPlatoService().findOne(plato.getId());
        if (plato.getVersion() != existingPlato.getVersion() && StringUtils.isEmpty(concurrencyControl)) {
            populateForm(model);
            model.addAttribute("plato", plato);
            model.addAttribute("concurrency", true);
            return new ModelAndView("platoes/edit");
        } else if (plato.getVersion() != existingPlato.getVersion() && "discard".equals(concurrencyControl)) {
            populateForm(model);
            model.addAttribute("plato", existingPlato);
            model.addAttribute("concurrency", false);
            return new ModelAndView("platoes/edit");
        } else if (plato.getVersion() != existingPlato.getVersion() && "apply".equals(concurrencyControl)) {
            // Update the version field to be able to override the existing values
            plato.setVersion(existingPlato.getVersion());
        }
        Plato savedPlato = getPlatoService().save(plato);
        UriComponents showURI = getItemLink().to(PlatoesItemThymeleafLinkFactory.SHOW).with("plato", savedPlato.getId()).toUri();
        return new ModelAndView("redirect:" + showURI.toUriString());
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param plato
     * @return ResponseEntity
     */
    @ResponseBody
    @DeleteMapping(name = "delete")
    public ResponseEntity<?> delete(@ModelAttribute Plato plato) {
        getPlatoService().delete(plato);
        return ResponseEntity.ok().build();
    }
}
