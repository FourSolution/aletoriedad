package com.aletoriedad.web;
import com.aletoriedad.model.Restaurante;
import com.aletoriedad.service.api.RestauranteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonObjectDeserializer;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.convert.ConversionService;
import org.springframework.roo.addon.web.mvc.controller.annotations.config.RooDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import io.springlets.web.NotFoundException;
import org.springframework.boot.jackson.JsonComponent;

/**
 * = RestauranteDeserializer
 *
 * TODO Auto-generated class documentation
 *
 */
@RooDeserializer(entity = Restaurante.class)
@JsonComponent
public class RestauranteDeserializer extends JsonObjectDeserializer<Restaurante> {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private RestauranteService restauranteService;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private ConversionService conversionService;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param restauranteService
     * @param conversionService
     */
    @Autowired
    public RestauranteDeserializer(@Lazy RestauranteService restauranteService, ConversionService conversionService) {
        this.restauranteService = restauranteService;
        this.conversionService = conversionService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return RestauranteService
     */
    public RestauranteService getRestauranteService() {
        return restauranteService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param restauranteService
     */
    public void setRestauranteService(RestauranteService restauranteService) {
        this.restauranteService = restauranteService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return ConversionService
     */
    public ConversionService getConversionService() {
        return conversionService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param conversionService
     */
    public void setConversionService(ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param jsonParser
     * @param context
     * @param codec
     * @param tree
     * @return Restaurante
     * @throws IOException
     */
    public Restaurante deserializeObject(JsonParser jsonParser, DeserializationContext context, ObjectCodec codec, JsonNode tree) {
        String idText = tree.asText();
        Long id = conversionService.convert(idText, Long.class);
        Restaurante restaurante = restauranteService.findOne(id);
        if (restaurante == null) {
            throw new NotFoundException("Restaurante not found");
        }
        return restaurante;
    }
}
