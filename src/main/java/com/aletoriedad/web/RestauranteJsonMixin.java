package com.aletoriedad.web;
import com.aletoriedad.model.Restaurante;
import org.springframework.roo.addon.web.mvc.controller.annotations.config.RooJsonMixin;
import com.aletoriedad.model.Plato;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Set;

/**
 * = RestauranteJsonMixin
 *
 * TODO Auto-generated class documentation
 *
 */
@RooJsonMixin(entity = Restaurante.class)
public abstract class RestauranteJsonMixin {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    @JsonIgnore
    private Set<Plato> platos;

    /**
     * TODO Auto-generated method documentation
     *
     * @return Set
     */
    public Set<Plato> getPlatos() {
        return platos;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param platos
     */
    public void setPlatos(Set<Plato> platos) {
        this.platos = platos;
    }
}
