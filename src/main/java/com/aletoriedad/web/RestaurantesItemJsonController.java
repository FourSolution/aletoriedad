package com.aletoriedad.web;
import com.aletoriedad.model.Restaurante;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.controller.annotations.responses.json.RooJSON;
import com.aletoriedad.service.api.RestauranteService;
import io.springlets.web.NotFoundException;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import org.springframework.web.util.UriComponents;

/**
 * = RestaurantesItemJsonController
 *
 * TODO Auto-generated class documentation
 *
 */
@RooController(entity = Restaurante.class, pathPrefix = "/api", type = ControllerType.ITEM)
@RooJSON
@RestController
@RequestMapping(value = "/api/restaurantes/{restaurante}", name = "RestaurantesItemJsonController", produces = MediaType.APPLICATION_JSON_VALUE)
public class RestaurantesItemJsonController {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private RestauranteService restauranteService;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param restauranteService
     */
    @Autowired
    public RestaurantesItemJsonController(RestauranteService restauranteService) {
        this.restauranteService = restauranteService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return RestauranteService
     */
    public RestauranteService getRestauranteService() {
        return restauranteService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param restauranteService
     */
    public void setRestauranteService(RestauranteService restauranteService) {
        this.restauranteService = restauranteService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @return Restaurante
     */
    @ModelAttribute
    public Restaurante getRestaurante(@PathVariable("restaurante") Long id) {
        Restaurante restaurante = restauranteService.findOne(id);
        if (restaurante == null) {
            throw new NotFoundException(String.format("Restaurante with identifier '%s' not found", id));
        }
        return restaurante;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param restaurante
     * @return ResponseEntity
     */
    @GetMapping(name = "show")
    public ResponseEntity<?> show(@ModelAttribute Restaurante restaurante) {
        return ResponseEntity.ok(restaurante);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param restaurante
     * @return UriComponents
     */
    public static UriComponents showURI(Restaurante restaurante) {
        return MvcUriComponentsBuilder.fromMethodCall(MvcUriComponentsBuilder.on(RestaurantesItemJsonController.class).show(restaurante)).buildAndExpand(restaurante.getId()).encode();
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param storedRestaurante
     * @param restaurante
     * @param result
     * @return ResponseEntity
     */
    @PutMapping(name = "update")
    public ResponseEntity<?> update(@ModelAttribute Restaurante storedRestaurante, @Valid @RequestBody Restaurante restaurante, BindingResult result) {
        if (result.hasErrors()) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(result);
        }
        restaurante.setId(storedRestaurante.getId());
        getRestauranteService().save(restaurante);
        return ResponseEntity.ok().build();
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param restaurante
     * @return ResponseEntity
     */
    @DeleteMapping(name = "delete")
    public ResponseEntity<?> delete(@ModelAttribute Restaurante restaurante) {
        getRestauranteService().delete(restaurante);
        return ResponseEntity.ok().build();
    }
}
