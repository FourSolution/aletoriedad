package com.aletoriedad.web;
import com.aletoriedad.model.Usuario;
import com.aletoriedad.service.api.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jackson.JsonObjectDeserializer;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.convert.ConversionService;
import org.springframework.roo.addon.web.mvc.controller.annotations.config.RooDeserializer;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import io.springlets.web.NotFoundException;
import org.springframework.boot.jackson.JsonComponent;

/**
 * = UsuarioDeserializer
 *
 * TODO Auto-generated class documentation
 *
 */
@RooDeserializer(entity = Usuario.class)
@JsonComponent
public class UsuarioDeserializer extends JsonObjectDeserializer<Usuario> {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private UsuarioService usuarioService;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private ConversionService conversionService;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param usuarioService
     * @param conversionService
     */
    @Autowired
    public UsuarioDeserializer(@Lazy UsuarioService usuarioService, ConversionService conversionService) {
        this.usuarioService = usuarioService;
        this.conversionService = conversionService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return UsuarioService
     */
    public UsuarioService getUsuarioService() {
        return usuarioService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param usuarioService
     */
    public void setUsuarioService(UsuarioService usuarioService) {
        this.usuarioService = usuarioService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return ConversionService
     */
    public ConversionService getConversionService() {
        return conversionService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param conversionService
     */
    public void setConversionService(ConversionService conversionService) {
        this.conversionService = conversionService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param jsonParser
     * @param context
     * @param codec
     * @param tree
     * @return Usuario
     * @throws IOException
     */
    public Usuario deserializeObject(JsonParser jsonParser, DeserializationContext context, ObjectCodec codec, JsonNode tree) {
        String idText = tree.asText();
        Long id = conversionService.convert(idText, Long.class);
        Usuario usuario = usuarioService.findOne(id);
        if (usuario == null) {
            throw new NotFoundException("Usuario not found");
        }
        return usuario;
    }
}
