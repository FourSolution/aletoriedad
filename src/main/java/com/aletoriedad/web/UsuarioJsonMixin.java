package com.aletoriedad.web;
import com.aletoriedad.model.Usuario;
import org.springframework.roo.addon.web.mvc.controller.annotations.config.RooJsonMixin;
import com.aletoriedad.model.CuponCompra;
import com.aletoriedad.model.Persona;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import java.util.Set;

/**
 * = UsuarioJsonMixin
 *
 * TODO Auto-generated class documentation
 *
 */
@RooJsonMixin(entity = Usuario.class)
public abstract class UsuarioJsonMixin {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    @JsonDeserialize(using = PersonaDeserializer.class)
    private Persona persona;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    @JsonIgnore
    private Set<CuponCompra> usuariocupon;

    /**
     * TODO Auto-generated method documentation
     *
     * @return Set
     */
    public Set<CuponCompra> getUsuariocupon() {
        return usuariocupon;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param usuariocupon
     */
    public void setUsuariocupon(Set<CuponCompra> usuariocupon) {
        this.usuariocupon = usuariocupon;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return Persona
     */
    public Persona getPersona() {
        return persona;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param persona
     */
    public void setPersona(Persona persona) {
        this.persona = persona;
    }
}
