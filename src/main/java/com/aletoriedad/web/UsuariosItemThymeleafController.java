package com.aletoriedad.web;
import com.aletoriedad.model.Usuario;
import org.springframework.roo.addon.web.mvc.controller.annotations.ControllerType;
import org.springframework.roo.addon.web.mvc.controller.annotations.RooController;
import org.springframework.roo.addon.web.mvc.thymeleaf.annotations.RooThymeleaf;
import com.aletoriedad.service.api.UsuarioService;
import io.springlets.web.NotFoundException;
import io.springlets.web.mvc.util.ControllerMethodLinkBuilderFactory;
import io.springlets.web.mvc.util.MethodLinkBuilderFactory;
import java.util.Locale;
import javax.validation.Valid;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponents;

/**
 * = UsuariosItemThymeleafController
 *
 * TODO Auto-generated class documentation
 *
 */
@RooController(entity = Usuario.class, type = ControllerType.ITEM)
@RooThymeleaf
@Controller
@RequestMapping(value = "/usuarios/{usuario}", name = "UsuariosItemThymeleafController", produces = MediaType.TEXT_HTML_VALUE)
public class UsuariosItemThymeleafController {

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private UsuarioService usuarioService;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private MethodLinkBuilderFactory<UsuariosItemThymeleafController> itemLink;

    /**
     * TODO Auto-generated attribute documentation
     *
     */
    private MessageSource messageSource;

    /**
     * TODO Auto-generated constructor documentation
     *
     * @param usuarioService
     * @param messageSource
     * @param linkBuilder
     */
    @Autowired
    public UsuariosItemThymeleafController(UsuarioService usuarioService, MessageSource messageSource, ControllerMethodLinkBuilderFactory linkBuilder) {
        setUsuarioService(usuarioService);
        setMessageSource(messageSource);
        setItemLink(linkBuilder.of(UsuariosItemThymeleafController.class));
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return UsuarioService
     */
    public UsuarioService getUsuarioService() {
        return usuarioService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param usuarioService
     */
    public void setUsuarioService(UsuarioService usuarioService) {
        this.usuarioService = usuarioService;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return MessageSource
     */
    public MessageSource getMessageSource() {
        return messageSource;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param messageSource
     */
    public void setMessageSource(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @return MethodLinkBuilderFactory
     */
    public MethodLinkBuilderFactory<UsuariosItemThymeleafController> getItemLink() {
        return itemLink;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param itemLink
     */
    public void setItemLink(MethodLinkBuilderFactory<UsuariosItemThymeleafController> itemLink) {
        this.itemLink = itemLink;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param id
     * @param locale
     * @param method
     * @return Usuario
     */
    @ModelAttribute
    public Usuario getUsuario(@PathVariable("usuario") Long id, Locale locale, HttpMethod method) {
        Usuario usuario = null;
        if (HttpMethod.PUT.equals(method)) {
            usuario = usuarioService.findOneForUpdate(id);
        } else {
            usuario = usuarioService.findOne(id);
        }
        if (usuario == null) {
            String message = messageSource.getMessage("error_NotFound", new Object[] { "Usuario", id }, "The record couldn't be found", locale);
            throw new NotFoundException(message);
        }
        return usuario;
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param usuario
     * @param model
     * @return ModelAndView
     */
    @GetMapping(name = "show")
    public ModelAndView show(@ModelAttribute Usuario usuario, Model model) {
        model.addAttribute("usuario", usuario);
        return new ModelAndView("usuarios/show");
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param usuario
     * @param model
     * @return ModelAndView
     */
    @GetMapping(value = "/inline", name = "showInline")
    public ModelAndView showInline(@ModelAttribute Usuario usuario, Model model) {
        model.addAttribute("usuario", usuario);
        return new ModelAndView("usuarios/showInline :: inline-content");
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param dataBinder
     */
    @InitBinder("usuario")
    public void initUsuarioBinder(WebDataBinder dataBinder) {
        dataBinder.setDisallowedFields("id");
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param model
     */
    public void populateFormats(Model model) {
        model.addAttribute("application_locale", LocaleContextHolder.getLocale().getLanguage());
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param model
     */
    public void populateForm(Model model) {
        populateFormats(model);
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param usuario
     * @param model
     * @return ModelAndView
     */
    @GetMapping(value = "/edit-form", name = "editForm")
    public ModelAndView editForm(@ModelAttribute Usuario usuario, Model model) {
        populateForm(model);
        model.addAttribute("usuario", usuario);
        return new ModelAndView("usuarios/edit");
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param usuario
     * @param version
     * @param concurrencyControl
     * @param result
     * @param model
     * @return ModelAndView
     */
    @PutMapping(name = "update")
    public ModelAndView update(@Valid @ModelAttribute Usuario usuario, @RequestParam("version") Integer version, @RequestParam(value = "concurrency", required = false, defaultValue = "") String concurrencyControl, BindingResult result, Model model) {
        // Check if provided form contain errors
        if (result.hasErrors()) {
            populateForm(model);
            return new ModelAndView("usuarios/edit");
        }
        // Concurrency control
        Usuario existingUsuario = getUsuarioService().findOne(usuario.getId());
        if (usuario.getVersion() != existingUsuario.getVersion() && StringUtils.isEmpty(concurrencyControl)) {
            populateForm(model);
            model.addAttribute("usuario", usuario);
            model.addAttribute("concurrency", true);
            return new ModelAndView("usuarios/edit");
        } else if (usuario.getVersion() != existingUsuario.getVersion() && "discard".equals(concurrencyControl)) {
            populateForm(model);
            model.addAttribute("usuario", existingUsuario);
            model.addAttribute("concurrency", false);
            return new ModelAndView("usuarios/edit");
        } else if (usuario.getVersion() != existingUsuario.getVersion() && "apply".equals(concurrencyControl)) {
            // Update the version field to be able to override the existing values
            usuario.setVersion(existingUsuario.getVersion());
        }
        Usuario savedUsuario = getUsuarioService().save(usuario);
        UriComponents showURI = getItemLink().to(UsuariosItemThymeleafLinkFactory.SHOW).with("usuario", savedUsuario.getId()).toUri();
        return new ModelAndView("redirect:" + showURI.toUriString());
    }

    /**
     * TODO Auto-generated method documentation
     *
     * @param usuario
     * @return ResponseEntity
     */
    @ResponseBody
    @DeleteMapping(name = "delete")
    public ResponseEntity<?> delete(@ModelAttribute Usuario usuario) {
        getUsuarioService().delete(usuario);
        return ResponseEntity.ok().build();
    }
}
