package com.aletoriedad.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QCuponCompra is a Querydsl query type for CuponCompra
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QCuponCompra extends EntityPathBase<CuponCompra> {

    private static final long serialVersionUID = -343458551L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QCuponCompra cuponCompra = new QCuponCompra("cuponCompra");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final QPlato plato;

    public final QUsuario usuario;

    public final NumberPath<Integer> version = createNumber("version", Integer.class);

    public QCuponCompra(String variable) {
        this(CuponCompra.class, forVariable(variable), INITS);
    }

    public QCuponCompra(Path<? extends CuponCompra> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QCuponCompra(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QCuponCompra(PathMetadata metadata, PathInits inits) {
        this(CuponCompra.class, metadata, inits);
    }

    public QCuponCompra(Class<? extends CuponCompra> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.plato = inits.isInitialized("plato") ? new QPlato(forProperty("plato"), inits.get("plato")) : null;
        this.usuario = inits.isInitialized("usuario") ? new QUsuario(forProperty("usuario"), inits.get("usuario")) : null;
    }

}

