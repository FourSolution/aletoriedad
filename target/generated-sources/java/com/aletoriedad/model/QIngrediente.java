package com.aletoriedad.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QIngrediente is a Querydsl query type for Ingrediente
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QIngrediente extends EntityPathBase<Ingrediente> {

    private static final long serialVersionUID = 402429250L;

    public static final QIngrediente ingrediente = new QIngrediente("ingrediente");

    public final StringPath descripcion = createString("descripcion");

    public final BooleanPath excluible = createBoolean("excluible");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath nombre = createString("nombre");

    public final SetPath<IngredientePlato, QIngredientePlato> platosingredientes = this.<IngredientePlato, QIngredientePlato>createSet("platosingredientes", IngredientePlato.class, QIngredientePlato.class, PathInits.DIRECT2);

    public final NumberPath<Integer> version = createNumber("version", Integer.class);

    public QIngrediente(String variable) {
        super(Ingrediente.class, forVariable(variable));
    }

    public QIngrediente(Path<? extends Ingrediente> path) {
        super(path.getType(), path.getMetadata());
    }

    public QIngrediente(PathMetadata metadata) {
        super(Ingrediente.class, metadata);
    }

}

