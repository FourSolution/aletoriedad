package com.aletoriedad.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QIngredientePlato is a Querydsl query type for IngredientePlato
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QIngredientePlato extends EntityPathBase<IngredientePlato> {

    private static final long serialVersionUID = 1020415742L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QIngredientePlato ingredientePlato = new QIngredientePlato("ingredientePlato");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final QIngrediente ingrediente;

    public final QPlato plato;

    public final NumberPath<Integer> version = createNumber("version", Integer.class);

    public QIngredientePlato(String variable) {
        this(IngredientePlato.class, forVariable(variable), INITS);
    }

    public QIngredientePlato(Path<? extends IngredientePlato> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QIngredientePlato(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QIngredientePlato(PathMetadata metadata, PathInits inits) {
        this(IngredientePlato.class, metadata, inits);
    }

    public QIngredientePlato(Class<? extends IngredientePlato> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.ingrediente = inits.isInitialized("ingrediente") ? new QIngrediente(forProperty("ingrediente")) : null;
        this.plato = inits.isInitialized("plato") ? new QPlato(forProperty("plato"), inits.get("plato")) : null;
    }

}

