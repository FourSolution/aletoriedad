package com.aletoriedad.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QPlato is a Querydsl query type for Plato
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QPlato extends EntityPathBase<Plato> {

    private static final long serialVersionUID = 893233710L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QPlato plato = new QPlato("plato");

    public final StringPath descripcion = createString("descripcion");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final SetPath<IngredientePlato, QIngredientePlato> ingredientesplatos = this.<IngredientePlato, QIngredientePlato>createSet("ingredientesplatos", IngredientePlato.class, QIngredientePlato.class, PathInits.DIRECT2);

    public final StringPath nombre = createString("nombre");

    public final SetPath<CuponCompra, QCuponCompra> platocupon = this.<CuponCompra, QCuponCompra>createSet("platocupon", CuponCompra.class, QCuponCompra.class, PathInits.DIRECT2);

    public final QRestaurante restaurante;

    public final NumberPath<Integer> version = createNumber("version", Integer.class);

    public QPlato(String variable) {
        this(Plato.class, forVariable(variable), INITS);
    }

    public QPlato(Path<? extends Plato> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QPlato(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QPlato(PathMetadata metadata, PathInits inits) {
        this(Plato.class, metadata, inits);
    }

    public QPlato(Class<? extends Plato> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.restaurante = inits.isInitialized("restaurante") ? new QRestaurante(forProperty("restaurante")) : null;
    }

}

