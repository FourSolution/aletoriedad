package com.aletoriedad.model;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QRestaurante is a Querydsl query type for Restaurante
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QRestaurante extends EntityPathBase<Restaurante> {

    private static final long serialVersionUID = -900732586L;

    public static final QRestaurante restaurante = new QRestaurante("restaurante");

    public final StringPath descripcion = createString("descripcion");

    public final StringPath direccion = createString("direccion");

    public final NumberPath<Long> id = createNumber("id", Long.class);

    public final StringPath nombre = createString("nombre");

    public final SetPath<Plato, QPlato> platos = this.<Plato, QPlato>createSet("platos", Plato.class, QPlato.class, PathInits.DIRECT2);

    public final NumberPath<Integer> version = createNumber("version", Integer.class);

    public QRestaurante(String variable) {
        super(Restaurante.class, forVariable(variable));
    }

    public QRestaurante(Path<? extends Restaurante> path) {
        super(path.getType(), path.getMetadata());
    }

    public QRestaurante(PathMetadata metadata) {
        super(Restaurante.class, metadata);
    }

}

